<?php
defined('BASEPATH') OR exit('No direct script access allowed');/**
 * class untuk handle survei
 * @author Feby Fitriani Sudarsono
 */
class about extends CI_Controller {
	 //constructor class
    public function __construct() {
      parent::__construct();
    }

	public function sejarah(){
		$this->load->model('Mlegislatif_model');
		$data1['legislatif'] = $this->Mlegislatif_model->get();
		$this->load->view('base/header.php',$data1);
		$this->load->view('base/sejarah.php');
		$this->load->view('base/footer.php');
	}

	public function naskah(){
		$this->load->model('Mlegislatif_model');
		$data1['legislatif'] = $this->Mlegislatif_model->get();
		$this->load->view('base/header.php',$data1);
		$this->load->view('base/naskah.php');
		$this->load->view('base/footer.php');
	}

	public function adart(){
		$this->load->model('Mlegislatif_model');
		$data1['legislatif'] = $this->Mlegislatif_model->get();
		$this->load->view('base/header.php',$data1);
		$this->load->view('base/adart.php');
		$this->load->view('base/footer.php');
	}

	public function visimisi(){
		$this->load->model('Mlegislatif_model');
		$data1['legislatif'] = $this->Mlegislatif_model->get();
		$this->load->view('base/header.php',$data1);
		$this->load->view('base/visimisi.php');
		$this->load->view('base/footer.php');
	}

	public function mabda(){
		$this->load->model('Mlegislatif_model');
		$data1['legislatif'] = $this->Mlegislatif_model->get();
		$this->load->view('base/header.php',$data1);
		$this->load->view('base/mabda.php');
		$this->load->view('base/footer.php');
	}

	public function makna(){
		$this->load->model('Mlegislatif_model');
		$data1['legislatif'] = $this->Mlegislatif_model->get();
		$this->load->view('base/header.php',$data1);
		$this->load->view('base/makna.php');
		$this->load->view('base/footer.php');
	}

	public function asas(){
		$this->load->model('Mlegislatif_model');
		$data1['legislatif'] = $this->Mlegislatif_model->get();
		$this->load->view('base/header.php',$data1);
		$this->load->view('base/asas.php');
		$this->load->view('base/footer.php');
	}

	public function struktur(){
		$this->load->model('Mlegislatif_model');
		$data1['legislatif'] = $this->Mlegislatif_model->get();
		$this->load->view('base/header.php',$data1);
		$this->load->view('base/struktur.php');
		$this->load->view('base/footer.php');
	}

	/*
	 * Get Detail
     * @author Feby Fitriani Sudarsono
	 *
	 * get data detail Survei
	 *
	 * @author	Feby Fitriani Sudarsono
	 * @access	public
	 * @return	void
	 */

}
?>