<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 /**
     * @author Feby Fitriani Sudarsono
	 * Model untuk select data untuk home
	 */
	 public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->helper(array('form', 'url', 'file','download'));
    }
	public function index()
	{
		// $this->cek_login();
		redirect(base_url("admin/post"));
		
	}
	public function kategori()
	{
		$this->cek_login();
		$this->load->model('Mkategori_model');
		$data['kategori'] = $this->Mkategori_model->get();
		$this->load->view('baseadmin/header.php');
		$this->load->view('form/kategori.php',$data);
		$this->load->view('baseadmin/footer.php');
	}
	public function post()
	{
		$this->cek_login();
		$this->load->model('Mkategori_model');
		$data['kategori'] = $this->Mkategori_model->get();
		$this->load->model('Mpost_model');
		$data['posting'] = $this->Mpost_model->get();
		$this->load->view('baseadmin/header.php');
		$this->load->view('form/posting.php',$data);
		$this->load->view('baseadmin/footer.php');
	}
	public function media()
	{
		$this->cek_login();
		$this->load->model('Mmedia_model');
		$data['media'] = $this->Mmedia_model->get();
		$this->load->view('baseadmin/header.php');
		$this->load->view('form/media.php',$data);
		$this->load->view('baseadmin/footer.php');
	}
	public function galeri()
	{
		$this->cek_login();
		$this->load->model('Mgaleri_model');
		$data['galeri'] = $this->Mgaleri_model->get();
		$this->load->view('baseadmin/header.php');
		$this->load->view('form/galeri.php',$data);
		$this->load->view('baseadmin/footer.php');
	}
	public function halaman_depan()
	{
		$this->cek_login();
		$this->load->model('Mhalaman_model');
		$data['halaman'] = $this->Mhalaman_model->get();
		$this->load->view('baseadmin/header.php');
		$this->load->view('form/halaman_depan.php',$data);
		$this->load->view('baseadmin/footer.php');
	}
	public function muser()
	{
		$this->cek_login();
		$this->load->model('Muser_model');
		$data['user'] = $this->Muser_model->get();
		$this->load->view('baseadmin/header.php');
		$this->load->view('form/muser.php',$data);
		$this->load->view('baseadmin/footer.php');
	}
	public function katalog()
	{
		$this->cek_login();
		$this->load->model('Mkatalog_model');
		$data['katalog'] = $this->Mkatalog_model->get("id_adm =".$this->session->userdata('id'));
		$this->load->view('baseadmin/header.php');
		$this->load->view('form/katalog.php',$data);
		$this->load->view('baseadmin/footer.php');
	}
	public function legislatif()
	{
		$this->cek_login();
		$this->load->model('Mlegislatif_model');
		$data['legislatif'] = $this->Mlegislatif_model->get();
		$this->load->view('baseadmin/header.php');
		$this->load->view('form/legislatif.php',$data);
		$this->load->view('baseadmin/footer.php');
	}
	public function pengurus()
	{
		$this->cek_login();
		$this->load->model('Mpengurus_model');
		$data['pengurus'] = $this->Mpengurus_model->get();
		$this->load->view('baseadmin/header.php');
		$this->load->view('form/pengurus.php',$data);
		$this->load->view('baseadmin/footer.php');
	}
	public function download()
	{
		$this->cek_login();
		$this->load->model('Mdownload_model');
		$data['download'] = $this->Mdownload_model->get();
		$this->load->view('baseadmin/header.php');
		$this->load->view('form/download.php',$data);
		$this->load->view('baseadmin/footer.php');
	}
	public function login(){
		if ($this->session->userdata('name')==null){
			$this->load->view('base/login.php');
		}
		else{
			$this->index();
		}
	}
	public function cek_login(){
		if ($this->session->userdata('name')==null){
			redirect(base_url("admin/login"));
		}	
	}
}
?>