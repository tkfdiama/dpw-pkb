<?php
defined('BASEPATH') OR exit('No direct script access allowed');/**
 * class untuk handle survei
 * @author Feby Fitriani Sudarsono
 */
class berita extends CI_Controller {
	 //constructor class
    public function __construct() {
      parent::__construct();
      $this->load->library('pagination');
		$this->db_evin = $this->load->database('captip', TRUE);
		$this->load->helper(array('form', 'url', 'file','download'));
    }

	public function headline(){
		$this->load->model('Mpost_model');
		$data['post'] = $this->Mpost_model->get("p.id_kat=2","p.up_post desc");
		$data['recent'] = $this->Mpost_model->get("p.id_kat=2","p.up_post desc","3");
		$this->load->model('Mlegislatif_model');
		$data1['legislatif'] = $this->Mlegislatif_model->get();
		$this->load->view('base/header.php',$data1);
		$this->load->view('base/berita.php', $data);
		$this->load->view('base/footer.php');
	}

	public function utama(){
		$this->load->model('Mpost_model');
		$data['post'] = $this->Mpost_model->get("p.id_kat=3","p.up_post desc");
		$data['recent'] = $this->Mpost_model->get("p.id_kat=3","p.up_post desc","3");
		$this->load->model('Mlegislatif_model');
		$data1['legislatif'] = $this->Mlegislatif_model->get();
		$this->load->view('base/header.php',$data1);
		$this->load->view('base/berita.php', $data);
		$this->load->view('base/footer.php');
	}

	public function parlemen(){
		$this->load->model('Mpost_model');
		$data['post'] = $this->Mpost_model->get("p.id_kat=4","p.up_post desc");
		$data['recent'] = $this->Mpost_model->get("p.id_kat=4","p.up_post desc","3");
		$this->load->model('Mlegislatif_model');
		$data1['legislatif'] = $this->Mlegislatif_model->get();
		$this->load->view('base/header.php',$data1);
		$this->load->view('base/berita.php', $data);
		$this->load->view('base/footer.php');
	}

	/*
	 * Get Detail
     * @author Feby Fitriani Sudarsono
	 *
	 * get data detail Survei
	 *
	 * @author	Feby Fitriani Sudarsono
	 * @access	public
	 * @return	void
	 */

}
?>