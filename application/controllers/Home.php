<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 /**
     * @author Feby Fitriani Sudarsono
	 * Model untuk select data untuk home
	 */
	 public function __construct() {
		parent::__construct();
		$this->load->library('pagination');
		$this->db_evin = $this->load->database('captip', TRUE);
		$this->load->helper(array('form', 'url', 'file','download'));
    }
	public function index()
	{
		$this->load->model('Mpost_model');
		$data['post'] = $this->Mpost_model->get("","up_post desc","6");
		$this->load->model('Mlegislatif_model');
		$data1['legislatif'] = $this->Mlegislatif_model->get();
		$this->load->model('Mpengurus_model');
		$data1['pengurus'] = $this->Mpengurus_model->get();
		$this->load->model('Mdownload_model');
		$data1['download'] = $this->Mdownload_model->get();
		$this->load->view('base/header.php',$data1);
		$this->load->view('base/home.php',$data);
		$this->load->view('base/footer.php');
	
	}

	
	public function gallery()
	{
		$config['base_url']=base_url()."/home/gallery";
    	$config['total_rows']= $this->db_evin->query("SELECT * FROM galeri;")->num_rows();
    	$config['per_page']=9;
   		$config['num_links'] = 3;
    	$config['uri_segment']=3;
 
        //Tambahan untuk styling
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tag_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tag_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tag_close'] = "</li>";
 
        $config['first_link']='first';
        $config['last_link']='last';
        $config['next_link']='> ';
        $config['prev_link']='< ';
        $this->pagination->initialize($config);
		$this->load->model('Mgaleri_model');
		$data['galeri'] = $this->Mgaleri_model->getAll($config);
		$this->load->model('Mlegislatif_model');
		$data1['legislatif'] = $this->Mlegislatif_model->get();
		$this->load->model('Mpengurus_model');
		$data1['pengurus'] = $this->Mpengurus_model->get();
		$this->load->model('Mdownload_model');
		$data1['download'] = $this->Mdownload_model->get();
		$this->load->view('base/header.php',$data1);
		$this->load->view('base/galery.php',$data);
		$this->load->view('base/footer.php');
	}
	
	public function post($id_post)
	{
		$this->load->model('Mpost_model');
		$data['recent'] = $this->Mpost_model->get("p.id_kat=3","p.up_post desc","3");
		$data['postnya'] = $this->Mpost_model->get("id_post = ".$id_post);
		$this->load->model('Mlegislatif_model');
		$data1['legislatif'] = $this->Mlegislatif_model->get();
		$this->load->model('Mpengurus_model');
		$data1['pengurus'] = $this->Mpengurus_model->get();
		$this->load->model('Mdownload_model');
		$data1['download'] = $this->Mdownload_model->get();
		$this->load->view('base/header.php',$data1);
		$this->load->view('form/postnya.php',$data);
		$this->load->view('base/footer.php');
	}
	
}
