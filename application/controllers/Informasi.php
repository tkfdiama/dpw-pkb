<?php
defined('BASEPATH') OR exit('No direct script access allowed');/**
 * class untuk handle survei
 * @author Feby Fitriani Sudarsono
 */
class informasi extends CI_Controller {
	 //constructor class
    public function __construct() {
      parent::__construct();
      //if(!$this->auth->validate(true)) exit(0);
      //$this->load->model('Mhalaman_model');
      $this->load->helper(array('form', 'url'));
    }

	public function dewan(){
		$this->load->view('base/header.php');
		$this->load->view('base/dewan.php');
		$this->load->view('base/footer.php');
	}

	public function ppid(){
		$this->load->view('base/header.php');
		$this->load->view('base/ppid.php');
		$this->load->view('base/footer.php');
	}


	public function informasi_publik(){
		$this->load->view('base/header.php');
		$this->load->view('base/informasi_publik.php');
		$this->load->view('base/footer.php');
	}
	public function lhkpn(){
		$this->load->view('base/header.php');
		$this->load->view('base/lhkpn');
		$this->load->view('base/footer.php');
	}

	public function keuangan(){
		$this ->load->view('base/header.php');
		$this->load->view('base/keuangan.php');
		$this->load->view('base/footer.php');

	}
	public function peraturan(){
		$this ->load->view('base/header.php');
		$this->load->view('base/peraturan.php');
		$this->load->view('base/footer.php');

	}
	public function dpr(){
		$this ->load->view('base/header.php');
		$this->load->view('base/dpr.php');
		$this->load->view('base/footer.php');
	}

	/*
	 * Get Detail
     * @author Feby Fitriani Sudarsono
	 *
	 * get data detail Survei
	 *
	 * @author	Feby Fitriani Sudarsono
	 * @access	public
	 * @return	void
	 */

}
?>