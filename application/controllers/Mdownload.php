<?php
/**
 * class untuk handle survei
 * @author Feby Fitriani Sudarsono
 */
class mdownload extends CI_Controller {
	 //constructor class
    public function __construct() {
      parent::__construct();
      //if(!$this->auth->validate(true)) exit(0);
      $this->load->model('Mdownload_model');
      $this->load->helper(array('form', 'url'));
    }

	public function index(){
  		// $this->load->model('Mpost_model');
		// $data['post'] = $this->Mpost_model->get('status_post = '.STATUS_ACTIVE);
		// $this->load->view('admin/index.php');
		// $this->load->view('admin/menu.php');
		// $this->load->view('admin/post.php',$data);
		// $this->load->view('admin/footer.php');
    redirect(base_url("admin/download"));
	}

	/*
	 * Get Detail
     * @author Feby Fitriani Sudarsono
	 *
	 * get data detail Survei
	 *
	 * @author	Feby Fitriani Sudarsono
	 * @access	public
	 * @return	void
	 */

  public function coba_insert(){   
      if($_POST['id_download'] == null || $_POST['id_download'] == ""){
      $nama = '0';
      if(!empty($_FILES['file']['tmp_name'])){ 
            $nama=time().$_FILES['file']['name'];
            move_uploaded_file($_FILES['file']['tmp_name'],"./assets/uploads/download/" . basename($nama));
        }
        $insert_id = $this->Mdownload_model->insert($nama);
        echo ("<script language='javascript'>alert('Data berhasil masuk');document.location='".base_url("home/download")."'</script>");
      }
      else {
      $nama = $_POST['fotonya'];
      if(!empty($_FILES['file']['tmp_name'])){ 
            unlink("./assets/uploads/download/$nama");
            $nama=time().$_FILES['file']['name'];
            move_uploaded_file($_FILES['file']['tmp_name'],"./assets/uploads/download/" . basename($nama));
        }
        $this->Mdownload_model->update($_POST['id_download'],$nama);
        echo ("<script language='javascript'>alert('Data berhasil diupdate');document.location='".base_url("home/download")."'</script>");
      }
  }
	public function get_detail($id_download)
	{
		if(!$this->input->is_ajax_request()) show_404();

		$detail = $this->Mdownload_model->get_by_id($id_download);
		if($detail != null) ajax_response('ok', NULL, $detail);
		else ajax_response('failed', 'Gagal');
	}

	/*
	 * Save method
     * @author Feby Fitriani Sudarsono
	 *
	 * insert/update survei data
	 *
	 * @author	Feby Fitriani Sudarsono
	 * @access	private
	 * @return	void
	 */



	/**
	 * Delete Survei
     * @author Feby Fitriani Sudarsono
	 *
	 * delete Survei data
	 *
	 * @author	Feby Fitriani Sudarsono
	 * @access	public
	 * @return	void
	 **/
	public function delete($id_download,$file){
		if(!$this->input->is_ajax_request()) show_404();

		if($id_download)
		{
			/* remove this if want use validate contraint
			if($this->violated_constraint($this->input->post('jns_id'))){
				ajax_response('failed', lang_value('jnsab_constraint_failed'));
			}*/
			//add_individual_data_log('Mjnssrt_model', $this->input->post('jns_id'), array('fld_uri'));
			$this->Mdownload_model->delete($id_download);
      if($file != 0)unlink("./assets/uploads/download/$file");
		}
		else
		{
			ajax_response('failed', 'Gagal');
		}
		ajax_response();
	}
}
?>