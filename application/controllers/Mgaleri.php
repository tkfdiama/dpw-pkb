<?php
/**
 * class untuk handle survei
 * @author Feby Fitriani Sudarsono
 */
class mgaleri extends CI_Controller {
	 //constructor class
    public function __construct() {
      parent::__construct();
      //if(!$this->auth->validate(true)) exit(0);
      $this->load->model('Mgaleri_model');
      $this->load->helper(array('form', 'url'));
    }

	public function index(){
  		// $this->load->model('Mgaleri_model');
		// $data['kategori'] = $this->Mgaleri_model->get('status_kategori = '.STATUS_ACTIVE);
		// $this->load->view('admin/index.php');
		// $this->load->view('admin/menu.php');
		// $this->load->view('admin/kategori.php',$data);
		// $this->load->view('admin/footer.php');
    redirect(base_url("admin/galeri"));
	}

	/*
	 * Get Detail
     * @author Feby Fitriani Sudarsono
	 *
	 * get data detail Survei
	 *
	 * @author	Feby Fitriani Sudarsono
	 * @access	public
	 * @return	void
	 */

  public function coba_insert(){   
      if($_POST['id_gal'] == null || $_POST['id_gal'] == ""){
      $nama = '0';
      if(!empty($_FILES['name_gal']['tmp_name'])){ 
            $nama=time().$_FILES['name_gal']['name'];
            move_uploaded_file($_FILES['name_gal']['tmp_name'],"./assets/uploads/galeri/" . basename($nama));
        }
        $insert_id = $this->Mgaleri_model->insert(
            $nama,$_POST['capt_gal'],$this->session->userdata('id')
            );
        echo ("<script language='javascript'>alert('Data berhasil masuk');document.location='".base_url("admin/galeri")."'</script>");
      }
      else {
      $nama = $_POST['fotonya'];
      if(!empty($_FILES['name_gal']['tmp_name'])){ 
            unlink("./assets/uploads/$nama");
            $nama=time().$_FILES['name_gal']['name'];
            move_uploaded_file($_FILES['name_gal']['tmp_name'],"./assets/uploads/galeri/" . basename($nama));
        }
        $this->Mgaleri_model->update($_POST['id_gal'],
            $nama,$_POST['capt_gal'],$this->session->userdata('id'));
        echo ("<script language='javascript'>alert('Data berhasil diupdate');document.location='".base_url("admin/galeri")."'</script>");
      }
  }
	public function get_detail($id_gal)
	{
		if(!$this->input->is_ajax_request()) show_404();

		$detail = $this->Mgaleri_model->get_by_id($id_gal);
		if($detail != null) ajax_response('ok', NULL, $detail);
		else ajax_response('failed', 'Gagal');
	}

	/*
	 * Save method
     * @author Feby Fitriani Sudarsono
	 *
	 * insert/update survei data
	 *
	 * @author	Feby Fitriani Sudarsono
	 * @access	private
	 * @return	void
	 */



	/**
	 * Delete Survei
     * @author Feby Fitriani Sudarsono
	 *
	 * delete Survei data
	 *
	 * @author	Feby Fitriani Sudarsono
	 * @access	public
	 * @return	void
	 **/
	public function delete($id_gal,$name_gal){
		if(!$this->input->is_ajax_request()) show_404();

		if($id_gal)
		{
			/* remove this if want use validate contraint
			if($this->violated_constraint($this->input->post('jns_id'))){
				ajax_response('failed', lang_value('jnsab_constraint_failed'));
			}*/
			//add_individual_data_log('Mjnssrt_model', $this->input->post('jns_id'), array('fld_uri'));
			$this->Mgaleri_model->delete($id_gal);
      if($name_gal != 0)unlink("./assets/uploads/galeri/$name_gal");
		}
		else
		{
			ajax_response('failed', 'Gagal');
		}
		ajax_response();
	}
}
?>