<?php
/**
 * class untuk handle survei
 * @author Feby Fitriani Sudarsono
 */
class mkategori extends CI_Controller {
	 //constructor class
    public function __construct() {
      parent::__construct();
      //if(!$this->auth->validate(true)) exit(0);
      $this->load->model('Mkategori_model');
      $this->load->helper(array('form', 'url'));
    }

	public function index(){
  		// $this->load->model('Mkategori_model');
		// $data['kategori'] = $this->Mkategori_model->get('status_kategori = '.STATUS_ACTIVE);
		// $this->load->view('admin/index.php');
		// $this->load->view('admin/menu.php');
		// $this->load->view('admin/kategori.php',$data);
		// $this->load->view('admin/footer.php');
    redirect(base_url("admin/kategori"));
	}

	/*
	 * Get Detail
     * @author Feby Fitriani Sudarsono
	 *
	 * get data detail Survei
	 *
	 * @author	Feby Fitriani Sudarsono
	 * @access	public
	 * @return	void
	 */

  public function coba_insert(){   
      if($_POST['id_kat'] == null || $_POST['id_kat'] == ""){
      $nama = '0';
        $insert_id = $this->Mkategori_model->insert(
            $_POST['nm_kat']);
        echo ("<script language='javascript'>alert('Data berhasil masuk');document.location='".base_url("admin/kategori")."'</script>");
      }
      else {
        $this->Mkategori_model->update($_POST['id_kat'],
            $_POST['nm_kat']);
        echo ("<script language='javascript'>alert('Data berhasil diupdate');document.location='".base_url("admin/kategori")."'</script>");
      }
  }
	public function get_detail($id_kat)
	{
		if(!$this->input->is_ajax_request()) show_404();

		$detail = $this->Mkategori_model->get_by_id($id_kat);
		if($detail != null) ajax_response('ok', NULL, $detail);
		else ajax_response('failed', 'Gagal');
	}

	/*
	 * Save method
     * @author Feby Fitriani Sudarsono
	 *
	 * insert/update survei data
	 *
	 * @author	Feby Fitriani Sudarsono
	 * @access	private
	 * @return	void
	 */



	/**
	 * Delete Survei
     * @author Feby Fitriani Sudarsono
	 *
	 * delete Survei data
	 *
	 * @author	Feby Fitriani Sudarsono
	 * @access	public
	 * @return	void
	 **/
	public function delete($id_kat){
		if(!$this->input->is_ajax_request()) show_404();

		if($id_kat)
		{
			/* remove this if want use validate contraint
			if($this->violated_constraint($this->input->post('jns_id'))){
				ajax_response('failed', lang_value('jnsab_constraint_failed'));
			}*/
			//add_individual_data_log('Mjnssrt_model', $this->input->post('jns_id'), array('fld_uri'));
			$this->Mkategori_model->delete($id_kat);
		}
		else
		{
			ajax_response('failed', 'Gagal');
		}
		ajax_response();
	}
}
?>