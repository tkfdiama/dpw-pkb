<?php
/**
 * class untuk handle survei
 * @author Feby Fitriani Sudarsono
 */
class mmedia extends CI_Controller {
	 //constructor class
    public function __construct() {
      parent::__construct();
      //if(!$this->auth->validate(true)) exit(0);
      $this->load->model('Mmedia_model');
      $this->load->helper(array('form', 'url'));
    }

	public function index(){
  		// $this->load->model('Mpost_model');
		// $data['post'] = $this->Mpost_model->get('status_post = '.STATUS_ACTIVE);
		// $this->load->view('admin/index.php');
		// $this->load->view('admin/menu.php');
		// $this->load->view('admin/post.php',$data);
		// $this->load->view('admin/footer.php');
    redirect(base_url("admin/media"));
	}

	/*
	 * Get Detail
     * @author Feby Fitriani Sudarsono
	 *
	 * get data detail Survei
	 *
	 * @author	Feby Fitriani Sudarsono
	 * @access	public
	 * @return	void
	 */

  public function coba_insert(){   
      if($_POST['id_press'] == null || $_POST['id_press'] == ""){
      $nama = '0';
      if(!empty($_FILES['ft_media']['tmp_name'])){ 
            $nama=time().$_FILES['ft_media']['name'];
            move_uploaded_file($_FILES['ft_media']['tmp_name'],"./assets/uploads/media/" . basename($nama));
        }
        $insert_id = $this->Mmedia_model->insert(
            $_POST['judul'],$_POST['isi'],
            $_POST['kategori'],$nama);
        echo ("<script language='javascript'>alert('Data berhasil masuk');document.location='".base_url("home/media")."'</script>");
      }
      else {
      $nama = $_POST['fotonya'];
      if(!empty($_FILES['ft_media']['tmp_name'])){ 
            unlink("./assets/uploads/media/$nama");
            $nama=time().$_FILES['ft_media']['name'];
            move_uploaded_file($_FILES['ft_media']['tmp_name'],"./assets/uploads/media/" . basename($nama));
        }
        $this->Mmedia_model->update($_POST['id_press'],
           	$_POST['judul'],$_POST['isi'],
            $_POST['kategori'],$nama);
        echo ("<script language='javascript'>alert('Data berhasil diupdate');document.location='".base_url("home/media")."'</script>");
      }
  }
	public function get_detail($id_press)
	{
		if(!$this->input->is_ajax_request()) show_404();

		$detail = $this->Mmedia_model->get_by_id($id_press);
		if($detail != null) ajax_response('ok', NULL, $detail);
		else ajax_response('failed', 'Gagal');
	}

	/*
	 * Save method
     * @author Feby Fitriani Sudarsono
	 *
	 * insert/update survei data
	 *
	 * @author	Feby Fitriani Sudarsono
	 * @access	private
	 * @return	void
	 */



	/**
	 * Delete Survei
     * @author Feby Fitriani Sudarsono
	 *
	 * delete Survei data
	 *
	 * @author	Feby Fitriani Sudarsono
	 * @access	public
	 * @return	void
	 **/
	public function delete($id_press,$ft_media){
		if(!$this->input->is_ajax_request()) show_404();

		if($id_press)
		{
			/* remove this if want use validate contraint
			if($this->violated_constraint($this->input->post('jns_id'))){
				ajax_response('failed', lang_value('jnsab_constraint_failed'));
			}*/
			//add_individual_data_log('Mjnssrt_model', $this->input->post('jns_id'), array('fld_uri'));
			$this->Mmedia_model->delete($id_press);
      if($ft_media != 0)unlink("./assets/uploads/media/$ft_media");
		}
		else
		{
			ajax_response('failed', 'Gagal');
		}
		ajax_response();
	}
}
?>