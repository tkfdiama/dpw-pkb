<?php
/**
 * class untuk handle survei
 * @author Feby Fitriani Sudarsono
 */
class mpengurus extends CI_Controller {
	 //constructor class
    public function __construct() {
      parent::__construct();
      //if(!$this->auth->validate(true)) exit(0);
      $this->load->model('Mpengurus_model');
      $this->load->helper(array('form', 'url'));
    }

	public function index(){
  		// $this->load->model('Mpost_model');
		// $data['post'] = $this->Mpost_model->get('status_post = '.STATUS_ACTIVE);
		// $this->load->view('admin/index.php');
		// $this->load->view('admin/menu.php');
		// $this->load->view('admin/post.php',$data);
		// $this->load->view('admin/footer.php');
    redirect(base_url("admin/pengurus"));
	}

	/*
	 * Get Detail
     * @author Feby Fitriani Sudarsono
	 *
	 * get data detail Survei
	 *
	 * @author	Feby Fitriani Sudarsono
	 * @access	public
	 * @return	void
	 */

  public function coba_insert(){   
      if($_POST['id_pengurus'] == null || $_POST['id_pengurus'] == ""){
      $nama = '0';
      if(!empty($_FILES['file']['tmp_name'])){ 
            $nama=time().$_FILES['file']['name'];
            move_uploaded_file($_FILES['file']['tmp_name'],"./assets/uploads/pengurus/" . basename($nama));
        }
        $insert_id = $this->Mpengurus_model->insert($nama);
        echo ("<script language='javascript'>alert('Data berhasil masuk');document.location='".base_url("home/pengurus")."'</script>");
      }
      else {
      $nama = $_POST['fotonya'];
      if(!empty($_FILES['file']['tmp_name'])){ 
            unlink("./assets/uploads/pengurus/$nama");
            $nama=time().$_FILES['file']['name'];
            move_uploaded_file($_FILES['file']['tmp_name'],"./assets/uploads/pengurus/" . basename($nama));
        }
        $this->Mpengurus_model->update($_POST['id_pengurus'],$nama);
        echo ("<script language='javascript'>alert('Data berhasil diupdate');document.location='".base_url("home/pengurus")."'</script>");
      }
  }
	public function get_detail($id_pengurus)
	{
		if(!$this->input->is_ajax_request()) show_404();

		$detail = $this->Mpengurus_model->get_by_id($id_pengurus);
		if($detail != null) ajax_response('ok', NULL, $detail);
		else ajax_response('failed', 'Gagal');
	}

	/*
	 * Save method
     * @author Feby Fitriani Sudarsono
	 *
	 * insert/update survei data
	 *
	 * @author	Feby Fitriani Sudarsono
	 * @access	private
	 * @return	void
	 */



	/**
	 * Delete Survei
     * @author Feby Fitriani Sudarsono
	 *
	 * delete Survei data
	 *
	 * @author	Feby Fitriani Sudarsono
	 * @access	public
	 * @return	void
	 **/
	public function delete($id_pengurus,$file){
		if(!$this->input->is_ajax_request()) show_404();

		if($id_pengurus)
		{
			/* remove this if want use validate contraint
			if($this->violated_constraint($this->input->post('jns_id'))){
				ajax_response('failed', lang_value('jnsab_constraint_failed'));
			}*/
			//add_individual_data_log('Mjnssrt_model', $this->input->post('jns_id'), array('fld_uri'));
			$this->Mpengurus_model->delete($id_pengurus);
      if($file != 0)unlink("./assets/uploads/pengurus/$file");
		}
		else
		{
			ajax_response('failed', 'Gagal');
		}
		ajax_response();
	}
}
?>