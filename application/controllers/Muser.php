<?php
/**
 * class untuk handle survei
 * @author Feby Fitriani Sudarsono
 */
class muser extends CI_Controller {
	 //constructor class
    public function __construct() {
      parent::__construct();
      //if(!$this->auth->validate(true)) exit(0);
      $this->load->model('Muser_model');
      $this->load->helper(array('form', 'url'));
    }

	public function index(){
  		// $this->load->model('Muser_model');
		// $data['muser'] = $this->Muser_model->get('status_muser = '.STATUS_ACTIVE);
		// $this->load->view('admin/index.php');
		// $this->load->view('admin/menu.php');
		// $this->load->view('admin/muser.php',$data);
		// $this->load->view('admin/footer.php');
    redirect(base_url("admin/muser"));
	}

	/*
	 * Get Detail
     * @author Feby Fitriani Sudarsono
	 *
	 * get data detail Survei
	 *
	 * @author	Feby Fitriani Sudarsono
	 * @access	public
	 * @return	void
	 */

  public function coba_insert(){   
      if($_POST['id_adm'] == null || $_POST['id_adm'] == ""){
      $nama = '0';
      if(!empty($_FILES['ft_adm']['tmp_name'])){ 
            $nama=time().$_FILES['ft_adm']['name'];
            move_uploaded_file($_FILES['ft_adm']['tmp_name'],"./assets/uploads/profil/" . basename($nama));
        }
        $insert_id = $this->Muser_model->insert(
            $_POST['username'],md5($_POST['password']),$nama);
        echo ("<script language='javascript'>alert('Data berhasil masuk');document.location='".base_url("home/muser")."'</script>");
      }
      else {
      $nama = $_POST['fotonya'];
      if(!empty($_FILES['ft_adm']['tmp_name'])){ 
            unlink("./assets/uploads/profil/$nama");
            $nama=time().$_FILES['ft_adm']['name'];
            move_uploaded_file($_FILES['ft_adm']['tmp_name'],"./assets/uploads/profil/" . basename($nama));
        }
        $this->Muser_model->update(
        	$_POST['id_adm'],$_POST['username'],$_POST['password'],$nama);
        echo ("<script language='javascript'>alert('Data berhasil diupdate');document.location='".base_url("home/muser")."'</script>");
      }
  }
	public function get_detail($id_adm)
	{
		if(!$this->input->is_ajax_request()) show_404();

		$detail = $this->Muser_model->get_by_id($id_adm);
		if($detail != null) ajax_response('ok', NULL, $detail);
		else ajax_response('failed', 'Gagal');
	}

	/*
	 * Save method
     * @author Feby Fitriani Sudarsono
	 *
	 * insert/update survei data
	 *
	 * @author	Feby Fitriani Sudarsono
	 * @access	private
	 * @return	void
	 */



	/**
	 * Delete Survei
     * @author Feby Fitriani Sudarsono
	 *
	 * delete Survei data
	 *
	 * @author	Feby Fitriani Sudarsono
	 * @access	public
	 * @return	void
	 **/
	public function delete($id_adm,$ft_adm){
		if(!$this->input->is_ajax_request()) show_404();

		if($id_adm)
		{
			/* remove this if want use validate contraint
			if($this->violated_constraint($this->input->post('jns_id'))){
				ajax_response('failed', lang_value('jnsab_constraint_failed'));
			}*/
			//add_individual_data_log('Mjnssrt_model', $this->input->post('jns_id'), array('fld_uri'));
			$this->Muser_model->delete($id_adm);
      if($ft_adm != 0)unlink("./assets/uploads/profil/$ft_adm");
		}
		else
		{
			ajax_response('failed', 'Gagal');
		}
		ajax_response();
	}
}
?>