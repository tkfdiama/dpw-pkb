<?php
/**
 * class untuk handle survei
 * @author Feby Fitriani Sudarsono
 */
class media extends CI_Controller {
	 //constructor class
    public function __construct() {
      parent::__construct();
      //if(!$this->auth->validate(true)) exit(0);
      $this->load->model('Mmedia_model');
      $this->load->helper(array('form', 'url'));
    }

	public function index(){
  		// $this->load->model('Mpost_model');
		// $data['post'] = $this->Mpost_model->get('status_post = '.STATUS_ACTIVE);
		// $this->load->view('admin/index.php');
		// $this->load->view('admin/menu.php');
		// $this->load->view('admin/post.php',$data);
		// $this->load->view('admin/footer.php');
    redirect(base_url("admin/mediat"));
	}

	public function press(){
		$this->load->model('Mmedia_model');
		$data['media'] = $this->Mmedia_model->get("m.kategori='press release'","m.up_media desc");
		$this->load->model('Mlegislatif_model');
		$data1['legislatif'] = $this->Mlegislatif_model->get();
		$this->load->view('base/header.php',$data1);
		$this->load->view('base/media.php',$data);
		$this->load->view('base/footer.php');
	}

	public function pidato(){
		$this->load->model('Mmedia_model');
		$data['media'] = $this->Mmedia_model->get("m.kategori='pidato'","m.up_media desc");
		$this->load->model('Mlegislatif_model');
		$data1['legislatif'] = $this->Mlegislatif_model->get();
		$this->load->view('base/header.php',$data1);
		$this->load->view('base/media.php',$data);
		$this->load->view('base/footer.php');
	}
}

?>