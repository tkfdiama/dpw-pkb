<?php

	class Mfoto_model extends CI_Model {

		/**
		 * @author Feby Fitriani Sudarsono
		 * Constructor class
		 */
		function __construct() {
			// Call the Model constructor
			parent::__construct();
			$this->db_evin = $this->load->database('captip', TRUE);
		}

		/**
		 * @author Feby Fitriani Sudarsono
		 * method untuk generate select query dari database
		 */
		public function select($selectcolumn=true){
	    	if($selectcolumn){
		    	$this->db_evin->select('id_ft');
		    	$this->db_evin->select('id_pst');
		    	$this->db_evin->select('nm_ft');
		    	// $this->db_evin->select('status_ktg');
	    	}
            	$this->db_evin->from('foto');
		}

		/**
         * @author Feby Fitriani Sudarsono
         * method untuk mendapatkan data dari tabel survei
         * @param type $limit jumlah yang mau diambil
         * @param type $offset mulai dari mana
         * @return type hasil query dari database
         */
        function get($where = "", $order = "id_ktg asc", $limit=null, $offset=null, $selectcolumn = true){
  			 $this->select($selectcolumn);
  			 if($limit != null) $this->db_evin->limit($limit, $offset);
  			 if($where != "") $this->db_evin->where($where);
  			 $this->db_evin->order_by($order);
  			 $query = $this->db_evin->get();
  			 return $query->result();
        }
        function get_by_id($id_ktg)
		 {
			if($id_ktg == null || trim($id_ktg) == "") return null;
			$result = $this->get("id_ktg = '".$id_ktg."'");
			return count($result) == 0?null:$result[0];
		 }

		/**
		 * @author Feby Fitriani Sudarsono
		 * Fungsi untuk insert data ke tabel survei
		 */
		function insert($nm_ktg=false,$ft_ktg=false)
		{
			$data = array();
			if($nm_ktg !== false)$data['nm_ktg'] = trim($nm_ktg);
			if($ft_ktg !== false)$data['ft_ktg'] = trim($ft_ktg);
      		$data['status_ktg']= STATUS_ACTIVE;
			$this->db_evin->insert('foto', $data);
			return $this->db_evin->insert_id();
		}

		function update($id_ktg=false,$nm_ktg=false,$ft_ktg=false)
		{
			$data = array();
      		if($nm_ktg !== false)$data['nm_ktg'] = trim($nm_ktg);
      		if($ft_ktg !== false)$data['ft_ktg'] = trim($ft_ktg);

			return $this->db_evin->update('foto', $data, "id_ktg = $id_ktg");
		}

		 /* @author Feby Fitriani Sudarsono
		 * Fungsi untuk delete data dari tabel Survei
		 */
		function delete($id_ktg)
		{
			$data = array();
			$data['status_ktg'] = STATUS_DELETE;
			return $this->db_evin->update('foto', $data, "id_ktg = $id_ktg");
		}

		/**
		 * @author Feby Fitriani Sudarsono
		 * Fungsi untuk menghitung jumlah row dari tabel survei
		 * @param type $where custome where
		 */
		function count_all($where = "")
		{
			if($where != null)$this->db_evin->where($where);
			return $this->db_evin->count_all_results('foto');
		}
	}
?>