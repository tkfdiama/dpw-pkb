<?php

	class Mgaleri_model extends CI_Model {

		/**
		 * @author Feby Fitriani Sudarsono
		 * Constructor class
		 */
		function __construct() {
			// Call the Model constructor
			parent::__construct();
			$this->db_evin = $this->load->database('captip', TRUE);
		}

		/**
		 * @author Feby Fitriani Sudarsono
		 * method untuk generate select query dari database
		 */
		public function select($selectcolumn=true){
	    	if($selectcolumn){
		    	$this->db_evin->select('id_gal');
		    	$this->db_evin->select('id_adm');
		    	$this->db_evin->select('name_gal');
		    	$this->db_evin->select('capt_gal');
		    	$this->db_evin->select('post_gal');
		    	// $this->db_evin->select('status_ktg');
	    	}
            	$this->db_evin->from('galeri');
		}

		/**
         * @author Feby Fitriani Sudarsono
         * method untuk mendapatkan data dari tabel survei
         * @param type $limit jumlah yang mau diambil
         * @param type $offset mulai dari mana
         * @return type hasil query dari database
         */
        function get($where = "", $order = "post_gal asc", $limit=null, $offset=null, $selectcolumn = true){
  			 $this->select($selectcolumn);
  			 if($limit != null) $this->db_evin->limit($limit, $offset);
  			 if($where != "") $this->db_evin->where($where);
  			 $this->db_evin->order_by($order);
  			 $query = $this->db_evin->get();
  			 return $query->result();
        }
        function get_by_id($id_gal)
		 {
			if($id_gal == null || trim($id_gal) == "") return null;
			$result = $this->get("id_gal = '".$id_gal."'");
			return count($result) == 0?null:$result[0];
		 }

		/**
		 * @author Feby Fitriani Sudarsono
		 * Fungsi untuk insert data ke tabel survei
		 */
		function insert($name_gal=false,$capt_gal=false,$id_adm=false)
		{
			$data = array();
			if($name_gal !== false)$data['name_gal'] = trim($name_gal);
			if($capt_gal !== false)$data['capt_gal'] = trim($capt_gal);
			if($id_adm !== false)$data['id_adm'] = trim($id_adm);
      		$data['post_gal']= now();
			$this->db_evin->insert('galeri', $data);
			return $this->db_evin->insert_id();
		}

		function update($id_gal=false,$name_gal=false,$capt_gal=false,$id_adm=false)
		{
			$data = array();
			if($name_gal !== false)$data['name_gal'] = trim($name_gal);
      		if($capt_gal !== false)$data['capt_gal'] = trim($capt_gal);
      		if($id_adm !== false)$data['id_adm'] = trim($id_adm);
      		$data['post_gal']= now();
			return $this->db_evin->update('galeri', $data, "id_gal = $id_gal");
		}

		 /* @author Feby Fitriani Sudarsono
		 * Fungsi untuk delete data dari tabel Survei
		 */
		function delete($id_gal)
		{
			$data = array();
			return $this->db_evin->delete('galeri',"id_gal = $id_gal");
		}

		/**
		 * @author Feby Fitriani Sudarsono
		 * Fungsi untuk menghitung jumlah row dari tabel survei
		 * @param type $where custome where
		 */
		function count_all($where = "")
		{
			if($where != null)$this->db_evin->where($where);
			return $this->db_evin->count_all_results('galeri');
		}
		function getAll($config){  
		    $this->db_evin->order_by("id_gal desc");
        	$hasilquery=$this->db_evin->get('galeri', $config['per_page'], $this->uri->segment(3));
        	if ($hasilquery->num_rows() > 0) {
            	foreach ($hasilquery->result() as $value) {
                	$data[]=$value;
           		}
            	return $data;
        	}
        	else{
        		return $data;
        	}      
    	}
	}
?>