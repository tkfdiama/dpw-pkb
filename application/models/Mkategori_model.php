<?php

	class Mkategori_model extends CI_Model {

		/**
		 * @author Feby Fitriani Sudarsono
		 * Constructor class
		 */
		function __construct() {
			// Call the Model constructor
			parent::__construct();
			$this->db_evin = $this->load->database('captip', TRUE);
		}

		/**
		 * @author Feby Fitriani Sudarsono
		 * method untuk generate select query dari database
		 */
		public function select($selectcolumn=true){
	    	if($selectcolumn){
		    	$this->db_evin->select('id_kat');
		    	$this->db_evin->select('id_adm');
		    	$this->db_evin->select('nm_kat');
	    	}
            	$this->db_evin->from('kategori');
		}

		/**
         * @author Feby Fitriani Sudarsono
         * method untuk mendapatkan data dari tabel survei
         * @param type $limit jumlah yang mau diambil
         * @param type $offset mulai dari mana
         * @return type hasil query dari database
         */
        function get($where = "", $order = "id_kat asc", $limit=null, $offset=null, $selectcolumn = true){
  			 $this->select($selectcolumn);
  			 if($limit != null) $this->db_evin->limit($limit, $offset);
  			 if($where != "") $this->db_evin->where($where);
  			 $this->db_evin->order_by($order);
  			 $query = $this->db_evin->get();
  			 return $query->result();
        }
        function get_by_id($id_kat)
		 {
			if($id_kat == null || trim($id_kat) == "") return null;
			$result = $this->get("id_kat = '".$id_kat."'");
			return count($result) == 0?null:$result[0];
		 }

		/**
		 * @author Feby Fitriani Sudarsono
		 * Fungsi untuk insert data ke tabel survei
		 */
		function insert($nm_kat=false,$id_adm=false)
		{
			$data = array();
			if($nm_kat !== false)$data['nm_kat'] = trim($nm_kat);
			if($id_adm !== false)$data['id_adm'] = trim($id_adm);
			$this->db_evin->insert('kategori', $data);
			return $this->db_evin->insert_id();
		}

		function update($id_kat=false,$nm_kat=false)
		{
			$data = array();
      		if($nm_kat !== false)$data['nm_kat'] = trim($nm_kat);

			return $this->db_evin->update('kategori', $data, "id_kat = $id_kat");
		}

		 /* @author Feby Fitriani Sudarsono
		 * Fungsi untuk delete data dari tabel Survei
		 */
		function delete($id_kat)
		{
			$data = array();
			return $this->db_evin->delete('kategori',"id_kat = $id_kat");
		}

		/**
		 * @author Feby Fitriani Sudarsono
		 * Fungsi untuk menghitung jumlah row dari tabel survei
		 * @param type $where custome where
		 */
		function count_all($where = "")
		{
			if($where != null)$this->db_evin->where($where);
			return $this->db_evin->count_all_results('kategori');
		}
	}
?>