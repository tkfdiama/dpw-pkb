<?php

	class mlegislatif_model extends CI_Model {

		/**
		 * @author Feby Fitriani Sudarsono
		 * Constructor class
		 */
		function __construct() {
			// Call the Model constructor
			parent::__construct();
			$this->db_evin = $this->load->database('captip', TRUE);
		}

		/**
		 * @author Feby Fitriani Sudarsono
		 * method untuk generate select query dari database
		 */
		public function select($selectcolumn=true){
	    	if($selectcolumn){
		    	$this->db_evin->select('l.id_legislatif');
		    	$this->db_evin->select('l.file');
		    	
	    	}
            	$this->db_evin->from('legislatif l');
		}

		/**
         * @author Feby Fitriani Sudarsono
         * method untuk mendapatkan data dari tabel survei
         * @param type $limit jumlah yang mau diambil
         * @param type $offset mulai dari mana
         * @return type hasil query dari database
         */
        function get($where = "", $order = "l.id_legislatif asc", $limit=null, $offset=null, $selectcolumn = true){
  			 $this->select($selectcolumn);
  			 if($limit != null) $this->db_evin->limit($limit, $offset);
  			 if($where != "") $this->db_evin->where($where);
  			 $this->db_evin->order_by($order);
  			 $query = $this->db_evin->get();
  			 return $query->result();
        }
        function get_by_id($id_post)
		 {
			if($id_legislatif == null || trim($id_legislatif) == "") return null;
			$result = $this->get("id_legislatif = '".$id_legislatif."'");
			return count($result) == 0?null:$result[0];
		 }

		/**
		 * @author Feby Fitriani Sudarsono
		 * Fungsi untuk insert data ke tabel survei
		 */
		function insert($file=false)
		{
			$data = array();
			if($file !== false)$data['file'] = trim($file);
			$this->db_evin->insert('legislatif', $data);
			return $this->db_evin->insert_id();
		}

		function update($id_legislatif=false, $file=false)
		{
			$data = array();
      		if($file !== false)$data['file'] = trim($file);

			return $this->db_evin->update('legislatif', $data, "id_legislatif = $id_legislatif");
		}

		 /* @author Feby Fitriani Sudarsono
		 * Fungsi untuk delete data dari tabel Survei
		 */
		function delete($id_legislatif)
		{
			return $this->db_evin->delete('legislatif', "id_legislatif = $id_legislatif");
		}

		/**
		 * @author Feby Fitriani Sudarsono
		 * Fungsi untuk menghitung jumlah row dari tabel survei
		 * @param type $where custome where
		 */
		function count_all($where = "")
		{
			if($where != null)$this->db_evin->where($where);
			return $this->db_evin->count_all_results('legislatif');
		}
	}
?>