<?php

	class Mmedia_model extends CI_Model {

		/**
		 * @author Feby Fitriani Sudarsono
		 * Constructor class
		 */
		function __construct() {
			// Call the Model constructor
			parent::__construct();
			$this->db_evin = $this->load->database('captip', TRUE);
		}

		/**
		 * @author Feby Fitriani Sudarsono
		 * method untuk generate select query dari database
		 */
		public function select($selectcolumn=true){
	    	if($selectcolumn){
		    	$this->db_evin->select('m.id_press');
		    	$this->db_evin->select('m.kategori');
		    	$this->db_evin->select('m.id_adm');
		    	$this->db_evin->select('m.judul');
		    	$this->db_evin->select('m.isi');
		    	$this->db_evin->select('m.up_media');
		    	$this->db_evin->select('m.ft_media');
	    	}
            	$this->db_evin->from('media_center m');
            	// $this->db_evin->join('kategori k','k.id_kat=p.id_kat');
		}

		/**
         * @author Feby Fitriani Sudarsono
         * method untuk mendapatkan data dari tabel survei
         * @param type $limit jumlah yang mau diambil
         * @param type $offset mulai dari mana
         * @return type hasil query dari database
         */
        function get($where = "", $order = "m.up_media asc", $limit=null, $offset=null, $selectcolumn = true){
  			 $this->select($selectcolumn);
  			 if($limit != null) $this->db_evin->limit($limit, $offset);
  			 if($where != "") $this->db_evin->where($where);
  			 $this->db_evin->order_by($order);
  			 $query = $this->db_evin->get();
  			 return $query->result();
        }
        function get_by_id($id_press)
		 {
			if($id_press == null || trim($id_press) == "") return null;
			$result = $this->get("id_press = '".$id_press."'");
			return count($result) == 0?null:$result[0];
		 }

		/**
		 * @author Feby Fitriani Sudarsono
		 * Fungsi untuk insert data ke tabel survei
		 */
		function insert($judul=false,$isi=false,$kategori=false,$ft_media=false)
		{
			$data = array();
			if($judul !== false)$data['judul'] = trim($judul);
			if($isi !== false)$data['isi'] = trim($isi);
			if($kategori !== false)$data['kategori'] = trim($kategori);
			if($ft_media !== false)$data['ft_media'] = trim($ft_media);
      		$data['up_media']= now();
      		$data['id_adm']= $this->session->userdata('id');
			$this->db_evin->insert('media_center', $data);
			return $this->db_evin->insert_id();
		}

		function update($id_press=false,$judul=false,$isi=false,$kategori=false,$ft_media=false)
		{
			$data = array();
      		if($judul !== false)$data['judul'] = trim($judul);
			if($isi !== false)$data['isi'] = trim($isi);
			if($kategori !== false)$data['kategori'] = trim($kategori);
			if($ft_media !== false)$data['ft_media'] = trim($ft_media);
      		$data['up_media']= now();

			return $this->db_evin->update('media_center', $data, "id_press = $id_press");
		}

		 /* @author Feby Fitriani Sudarsono
		 * Fungsi untuk delete data dari tabel Survei
		 */
		function delete($id_press)
		{
			return $this->db_evin->delete('media_center', "id_press = $id_press");
		}

		/**
		 * @author Feby Fitriani Sudarsono
		 * Fungsi untuk menghitung jumlah row dari tabel survei
		 * @param type $where custome where
		 */
		function count_all($where = "")
		{
			if($where != null)$this->db_evin->where($where);
			return $this->db_evin->count_all_results('media_center');
		}
	}
?>