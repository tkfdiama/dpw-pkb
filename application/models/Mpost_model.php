<?php

	class Mpost_model extends CI_Model {

		/**
		 * @author Feby Fitriani Sudarsono
		 * Constructor class
		 */
		function __construct() {
			// Call the Model constructor
			parent::__construct();
			$this->db_evin = $this->load->database('captip', TRUE);
		}

		/**
		 * @author Feby Fitriani Sudarsono
		 * method untuk generate select query dari database
		 */
		public function select($selectcolumn=true){
	    	if($selectcolumn){
		    	$this->db_evin->select('p.id_post');
		    	$this->db_evin->select('p.id_adm');
		    	$this->db_evin->select('p.id_kat');
		    	$this->db_evin->select('p.jdl_post');
		    	$this->db_evin->select('p.isi_post');
		    	$this->db_evin->select('p.ft_post');
		    	$this->db_evin->select('p.up_post');
		    	$this->db_evin->select('k.nm_kat');
	    	}
            	$this->db_evin->from('post p');
            	$this->db_evin->join('kategori k','k.id_kat=p.id_kat');
		}

		/**
         * @author Feby Fitriani Sudarsono
         * method untuk mendapatkan data dari tabel survei
         * @param type $limit jumlah yang mau diambil
         * @param type $offset mulai dari mana
         * @return type hasil query dari database
         */
        function get($where = "", $order = "p.up_post asc", $limit=null, $offset=null, $selectcolumn = true){
  			 $this->select($selectcolumn);
  			 if($limit != null) $this->db_evin->limit($limit, $offset);
  			 if($where != "") $this->db_evin->where($where);
  			 $this->db_evin->order_by($order);
  			 $query = $this->db_evin->get();
  			 return $query->result();
        }
        function get_by_id($id_post)
		 {
			if($id_post == null || trim($id_post) == "") return null;
			$result = $this->get("id_post = '".$id_post."'");
			return count($result) == 0?null:$result[0];
		 }

		/**
		 * @author Feby Fitriani Sudarsono
		 * Fungsi untuk insert data ke tabel survei
		 */
		function insert($jdl_post=false,$isi_post=false,$id_kat=false,$ft_post=false)
		{
			$data = array();
			if($jdl_post !== false)$data['jdl_post'] = trim($jdl_post);
			if($isi_post !== false)$data['isi_post'] = trim($isi_post);
			if($id_kat !== false)$data['id_kat'] = trim($id_kat);
			if($ft_post !== false)$data['ft_post'] = trim($ft_post);
      		$data['up_post']= now();
      		$data['id_adm']= $this->session->userdata('id');
			$this->db_evin->insert('post', $data);
			return $this->db_evin->insert_id();
		}

		function update($id_post=false,$jdl_post=false,$isi_post=false,$id_kat=false,$ft_post=false)
		{
			$data = array();
      		if($jdl_post !== false)$data['jdl_post'] = trim($jdl_post);
			if($isi_post !== false)$data['isi_post'] = trim($isi_post);
			if($id_kat !== false)$data['id_kat'] = trim($id_kat);
			if($ft_post !== false)$data['ft_post'] = trim($ft_post);
      		$data['up_post']= now();

			return $this->db_evin->update('post', $data, "id_post = $id_post");
		}

		 /* @author Feby Fitriani Sudarsono
		 * Fungsi untuk delete data dari tabel Survei
		 */
		function delete($id_post)
		{
			return $this->db_evin->delete('post', "id_post = $id_post");
		}

		/**
		 * @author Feby Fitriani Sudarsono
		 * Fungsi untuk menghitung jumlah row dari tabel survei
		 * @param type $where custome where
		 */
		function count_all($where = "")
		{
			if($where != null)$this->db_evin->where($where);
			return $this->db_evin->count_all_results('post');
		}
	}
?>