<?php

	class Muser_model extends CI_Model {

		/**
		 * @author Feby Fitriani Sudarsono
		 * Constructor class
		 */
		function __construct() {
			// Call the Model constructor
			parent::__construct();
			$this->db_evin = $this->load->database('captip', TRUE);
		}

		/**
		 * @author Feby Fitriani Sudarsono
		 * method untuk generate select query dari database
		 */
		public function select($selectcolumn=true){
	    	if($selectcolumn){
		    	$this->db_evin->select('id_adm');
		    	$this->db_evin->select('username');
		    	$this->db_evin->select('password');
		    	$this->db_evin->select('ft_adm');
		    	$this->db_evin->select('status_adm');
		    	$this->db_evin->select('last_used');
	    	}
            	$this->db_evin->from('admin');
		}

		/**
         * @author Feby Fitriani Sudarsono
         * method untuk mendapatkan data dari tabel survei
         * @param type $limit jumlah yang mau diambil
         * @param type $offset mulai dari mana
         * @return type hasil query dari database
         */
        function get($where = "", $order = "id_adm asc", $limit=null, $offset=null, $selectcolumn = true){
  			 $this->select($selectcolumn);
  			 if($limit != null) $this->db_evin->limit($limit, $offset);
  			 if($where != "") $this->db_evin->where($where);
  			 $this->db_evin->order_by($order);
  			 $query = $this->db_evin->get();
  			 return $query->result();
        }
        function get_by_id($id_adm)
		 {
			if($id_adm == null || trim($id_adm) == "") return null;
			$result = $this->get("id_adm = '".$id_adm."'");
			return count($result) == 0?null:$result[0];
		 }

		/**
		 * @author Feby Fitriani Sudarsono
		 * Fungsi untuk insert data ke tabel survei
		 */
		function insert($username=false,$password=false,$ft_adm=false)
		{
			$data = array();
			if($username !== false)$data['username'] = trim($username);
			if($password !== false)$data['password'] = trim($password);
			if($ft_adm !== false)$data['ft_adm'] = trim($ft_adm);
      		$data['status_adm']= STATUS_ACTIVE;
			$this->db_evin->insert('admin', $data);
			return $this->db_evin->insert_id();
		}

		function update($id_adm=false,$username=false,$password=false,$ft_adm=false)
		{
			$data = array();
			if($username !== false)$data['username'] = trim($username);
			if($password !== false)$data['password'] = trim($password);
			if($ft_adm !== false)$data['ft_adm'] = trim($ft_adm);

			return $this->db_evin->update('admin', $data, "id_adm = $id_adm");
		}

		 /* @author Feby Fitriani Sudarsono
		 * Fungsi untuk delete data dari tabel Survei
		 */
		function delete($id_adm)
		{
			$data = array();
			$data['status_adm'] = STATUS_DELETE;
			return $this->db_evin->update('admin', $data, "id_adm = $id_adm");
		}

		function last_sign($id_adm)
		{
			$data = array();
			$data['last_used'] = now();
			return $this->db_evin->update('admin', $data, "id_adm = $id_adm");
		}

		/**
		 * @author Feby Fitriani Sudarsono
		 * Fungsi untuk menghitung jumlah row dari tabel survei
		 * @param type $where custome where
		 */
		function count_all($where = "")
		{
			if($where != null)$this->db_evin->where($where);
			return $this->db_evin->count_all_results('admin');
		}
	}
?>