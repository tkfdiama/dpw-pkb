    <!-- breadcumb-area start -->
    <div class="breadcumb-area black-opacity bg-img-2">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                        <h2>Asas dan Prinsip Perjuangan</h2>
                        <ul>
                            <li><a href="<?php echo base_url();?>home">Home/</a></li>
                            <li>Tentang PKB/</li>
                            <li>Asas dan Prinsip Perjuangan</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcumb-area end -->
    <!-- blog-area start -->
    <div class="blog-area blog-details-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="blog-wrap">
                        <div class="blog-content">
                            <h4>Asas dan Prinsip Perjuangan</h4>
                        </div>
                    </div>
                    <div class="blog-details-wrap">
                        <blockquote style="text-align:justify"><b>Asas Perjuangan</b><br> Asas Partai Kebangkitan Bangsa berasaskan Ketuhanan Yang Maha Esa, kemanusiaan yang adil dan beradab, persatuan Indonesia, kerakyatan yang dipimpin oleh hikmah kebijakan dalam permusyawaratan/perwakilan, dan keadilan sosial bagi seluruh rakyat Indondesia.</blockquote>
						<blockquote style="text-align:justify"><b>Prinsip Perjuangan</b><br> Prinsip perjuangan Partai Kebangkitan Bangsa adalah pengabdian kepada Allah Subhanahu wa Ta'ala, menjunjung tinggi kebenaran dan kejujuran, menegakkan keadilan, menjaga persatuan, menumbuhkan persaudaraan dan kebersamaan sesuai dengan nilai-nilai Islam Ahlusunnah Waljamaah.</blockquote>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- blog-area end -->