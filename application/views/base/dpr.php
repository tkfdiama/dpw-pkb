    <!-- breadcumb-area start -->
    <div class="breadcumb-area black-opacity bg-img-2">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                        <h2>DPR-RI</h2>
                        <ul>
                            <li><a href="<?php echo base_url();?>home">Home/</a></li>
                            <li>Informasi</li>
                            <li>DPR-RI</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcumb-area end -->
    <!-- blog-area start -->
    <div class="blog-area blog-details-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="blog-wrap">
                        
                    </div>
                    <div class="blog-details-wrap">
                       
                       <a href="<?php echo base_url();?>assets/doc/DPR.pdf" target="_blank">DPR-RI</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- blog-area end -->