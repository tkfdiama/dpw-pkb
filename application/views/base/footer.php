<!-- footer-area start -->
    <footer class="footer-area">
        <div class="footer-top bg-1">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="footer-widget footer-logo">
                          <!--  <img src="<?php echo base_url();?>assets/img/icon.png" alt="">
                            <p>Dewan Pengurus Wilayah PKB Jawa Timur.</p>
                         <-->   <ul>
                         <a href="https://info.flagcounter.com/Usmj"><img src="https://s11.flagcounter.com/count2/Usmj/bg_FFFFFF/txt_000000/border_CCCCCC/columns_2/maxflags_10/viewers_0/labels_0/pageviews_0/flags_0/percent_0/" alt="Flag Counter" border="0"></a>
                        

<!--                                 <li><a href="https://www.instagram.com/dpwpkbjatim/"><i class="fa fa-instagram"></i></a></li>
 -->                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="footer-widget footer-contact">
                            <h4 class="widget-title">Kontak</h4>
                            <ul>
                                <li><i class="fa fa-home"></i> JL. Ketintang Madya, 153 - 155, Karah, Jambangan, Kota SBY, Jawa Timur 60232</li>
                                <li><i class="fa fa-phone"></i> (031) 8280033</li>
                                <li><i class="fa fa-envelope-o"></i> dpwpkbjawatimur@gmail.com</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-12">
                        <div class="footer-widget footer-menu">
                            <h4 class="widget-title">Sosial Media</h4>
                            <ul>
                                <a href="https://www.instagram.com/dpwpkbjatim/" class="fa fa-instagram" style="font-size: 40px;"></a>
                                <a href=https://www.facebook.com/dpwpkbjatim/ class="fa fa-facebook" style="font-size: 40px;"></a>
                                <a href=https://twitter.com/@dpwpkbjatim_ class="fa fa-twitter" style="font-size:40px;"></a>
                                <a href=https://www.youtube.com/channel/UCHCCo4-jtUAX6pYvwEiYIdw class="fa fa-youtube" style="font-size:40px;"></a>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- <div class="footer-bootem">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <p>&copy; --> <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
<!-- Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a> -->
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                    <!-- </div>
                </div>
            </div>
        </div> -->
    </footer>
    <!-- footer-area start -->
    <!-- jquery latest version -->
    <script src="<?php echo base_url();?>assets/home/js/vendor/jquery-2.2.4.min.js"></script>
    <!-- popper.min.js -->
    <script src="<?php echo base_url();?>assets/home/js/vendor/popper.min.js"></script>
    <!-- bootstrap js -->
    <script src="<?php echo base_url();?>assets/home/js/bootstrap.min.js"></script>
    <!-- owl.carousel.2.0.0-beta.2.4 css -->
    <script src="<?php echo base_url();?>assets/home/js/owl.carousel.min.js"></script>
    <!-- slick.min.js -->
    <script src="<?php echo base_url();?>assets/home/js/slick.min.js"></script>
    <!-- plugins js -->
    <script src="<?php echo base_url();?>assets/home/js/plugins.js"></script>
    <!-- google map -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCbeBYsZSDkbIyfUkoIw1Rt38eRQOQQU0o"></script>
    <script>
    function initialize() {
        var mapOptions = {
            zoom: 15,
            scrollwheel: false,
            center: new google.maps.LatLng(40.712764, -74.005667),
            styles: [{
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#f5f5f5"
                    }]
                },
                {
                    "elementType": "labels.icon",
                    "stylers": [{
                        "visibility": "off"
                    }]
                },
                {
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#616161"
                    }]
                },
                {
                    "elementType": "labels.text.stroke",
                    "stylers": [{
                        "color": "#f5f5f5"
                    }]
                },
                {
                    "featureType": "administrative.land_parcel",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#bdbdbd"
                    }]
                },
                {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#eeeeee"
                    }]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#757575"
                    }]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#e5e5e5"
                    }]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#9e9e9e"
                    }]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#ffffff"
                    }]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#757575"
                    }]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#dadada"
                    }]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#616161"
                    }]
                },
                {
                    "featureType": "road.local",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#9e9e9e"
                    }]
                },
                {
                    "featureType": "transit.line",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#e5e5e5"
                    }]
                },
                {
                    "featureType": "transit.station",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#eeeeee"
                    }]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#c9c9c9"
                    }]
                },
                {
                    "featureType": "water",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#9e9e9e"
                    }]
                }
            ]
        };

        var map = new google.maps.Map(document.getElementById('googleMap'),
            mapOptions);


        var marker = new google.maps.Marker({
            position: map.getCenter(),
            animation: google.maps.Animation.BOUNCE,
            map: map
        });

    }

    google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    <!-- main js -->
    <script src="<?php echo base_url();?>assets/home/js/scripts.js"></script>
</body>

</html>