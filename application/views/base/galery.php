    <!-- breadcumb-area start -->
    <div class="breadcumb-area black-opacity bg-img-2">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                        <h2>Galeri Kami</h2>
                        <ul>
                            <li><a href="<?php echo base_url();?>home">Home</a></li>
                            <li>/</li>
                            <li>Galeri</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcumb-area end -->    
    <!-- .project-area start -->
    <div class="project-area">
        <div class="container">
            <div class="row grid">
            <?php foreach ($galeri as $row): ?>
            	<div class="col-lg-3 col-sm-6 col-12 project cat2 cat3">
                    <div class="project-wrap">
                        <img src="<?php echo base_url();?>assets/uploads/galeri/<?php echo $row->name_gal;?>" alt="">
                        <div class="project-content">
                        	<span><?php echo $row->post_gal;?></span>
                            <a href="<?php echo base_url();?>assets/uploads/galeri/<?php echo $row->name_gal;?>" class="popup"><i class="fa fa-search"></i></a>
                            <h3><?php echo $row->capt_gal;?></h3>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
        </div>
    </div>
    <!-- .project-area end -->
<!--                 <div class="col-12">
                    <div class="pagination-wrap text-center">
                        <ul>
                            <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        </ul>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
    <!-- blog-area end -->
