<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>DPW PKB JAWA TIMUR</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url();?>assets/img/favicon.png">
    <!-- Place favicon.ico in the root directory -->
    <!-- all css here -->
    <!-- bootstrap v3.3.7 css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/home/css/bootstrap.min.css">
    <!-- animate css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/home/css/animate.css">
    <!-- owl.carousel.2.0.0-beta.2.4 css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/home/css/owl.carousel.css">
    <!-- font-awesome v4.6.3 css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/home/css/font-awesome.min.css">
    <!-- flaticon.css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/home/css/flaticon.css">
    <!-- magnific-popup.css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/home/css/magnific-popup.css">
    <!-- slicknav.min.css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/home/css/slicknav.min.css">
    <!-- slicknav.min.css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/home/css/slick.css">
    <!-- style css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/home/css/styles.css">
    <!-- responsive css -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/home/css/responsive.css">
    <!-- modernizr css -->
    <script src="<?php echo base_url();?>assets/home/js/vendor/modernizr-2.8.3.min.js"></script>
</head>
<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- header-area start -->
    <header class="header-area bg-1" id="sticky-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-9 col-sm-12 col-6">
                    <div class="logo">
                        <a href="<?php echo base_url();?>home"><img src="<?php echo base_url();?>assets/img/icon.png" alt=""></a>
                        <strong>&nbsp; DPW PKB JAWA TIMUR</strong>
                    </div>
                </div>
                <div class="col-lg-8 d-none d-lg-block">
                    <div class="mainmenu">
                        <ul id="navigation">
                            <li class="active"><a href="<?php echo base_url();?>home">Home </a>
                            </li>
                            <li><a href="javascript:void(0);">Berita <i class="fa fa-angle-down"></i></a>
                                <ul>
                                    <li><a href="<?php echo base_url();?>berita/headline">Headline</a></li>
                                    <li><a href="<?php echo base_url();?>berita/utama">Utama</a></li>
                                    <li><a href="<?php echo base_url();?>berita/parlemen">Parlemen</a></li>
                                </ul>
                            <li><a href="javascript:void(0);">Media<i class="fa fa-angle-down"></i></a>
                                <ul>
                                    <li><a href="<?php echo base_url();?>media/press">Press Release</a></li>
                                    <li><a href="<?php echo base_url();?>media/pidato">Pidato DPW PKB Jatim</a></li>
                                    <li><a href="<?php echo base_url();?>home/gallery">Foto</a></li>
                                </ul>
                            <li><a href="javascript:void(0);">Informasi<i class="fa fa-angle-down"></i></a>
                                <ul>
                                    <?php foreach ($pengurus as $row):?>
                                    <li><a href="<?php echo base_url();?>assets/uploads/pengurus/<?php echo$row->file;?>">Dewan Pengurus</a></li>
                                    <?php endforeach ?>
                                    <?php foreach ($legislatif as $row):?>
                                    <li><a href="<?php echo base_url();?>assets/uploads/legislatif/<?php echo$row->file;?>">Anggota Legislatif</a></li>
                                    <?php endforeach ?>
                                    <li><a href="<?php echo base_url();?>assets/doc/peraturan.pdf">Peraturan Partai</a></li>
                                </ul> 
                            </li>
                           
                            </li>
                            <li><a href="javascript:void(0);">Download <i class="fa fa-angle-down"></i></a>
                                <ul>
                                     <?php foreach ($download as $row):?>
                                 <li><a href="<?php echo base_url();?>assets/uploads/download/<?php echo$row->file;?>">Donwload File</a></li>
                                    <?php endforeach ?>
                                <li><a href="<?php echo base_url();?>home/gallery">Kumpulan Lagu</a></li>
                                </ul>
                            </li>
                            <li><a href="javascript:void(0);">Tentang PKB <i class="fa fa-angle-down"></i></a>
                                <ul>
                                    <li><a href="<?php echo base_url();?>about/sejarah">Sejarah Pendirian</a></li>
                                    <li><a href="<?php echo base_url();?>about/naskah">Naskah Deklarasi</a></li>
                                    <li><a href="<?php echo base_url();?>assets/doc/adart.pdf">AD/ART</a></li>
                                    <li><a href="<?php echo base_url();?>about/mabda">Mabda' Siyasi</a></li>
                                    <li><a href="<?php echo base_url();?>about/visimisi">Visi dan Misi</a></li>
                                    <li><a href="<?php echo base_url();?>about/makna">Makna Lambang</a></li>
                                    <li><a href="<?php echo base_url();?>about/asas">Asas dan Prinsip perjuangan</a></li>
                                    <li><a href="<?php echo base_url();?>about/struktur">Struktur Organisasi</a></li>
                                </ul>
                            </li>
                            
                             
                    </div>
                </div>
                <div class="col-lg-1 col-md-2 col-sm-3 col-4">
                </div>
                <div class="col-md-1 col-sm-1 col-2 d-lg-none d-sm-block">
                    <div class="responsive-menu-wrap floatright"></div>
                </div>
            </div>
        </div>
    </header>
    <!-- header-area end -->