    <!-- slider-area start -->
    <div class="slider-area">
        <div class="slider-active next-prev-style">
            <div class="slider-items">
                <img src="<?php echo base_url();?>assets/img/bg.png" alt="" class="slider">
                <div class="slider-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="slider-text">
                                    <div class="line"></div>
                                    <h2><span class="d-block">DPW PKB JAWA TIMUR</span> <span class="color">Home</span> </h2>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slider-items">
                <img src="<?php echo base_url();?>assets/img/bg.png" alt="" class="slider">
                <div class="slider-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="slider-text">
                                   
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- slider-area end -->
    <!-- testmonial-area start -->
    
    <!-- testmonial-area end -->
    <!-- service-area start -->
    <div class="service-area bg-1">
        <div class="container">
            <div class="row">
                <div class="col-12" style="padding-top: -1000%; padding-bottom: -500%">
                    <div class="section-title text-center">
                        <h2>Berita</h2> 
                    </div>
                </div>
            </div>
            <div class="row">
            <?php foreach(array_slice($post, 0, 6) as $row ): ?>
                <div class="col-sm-6 col-12 col-lg-4">
                    <div class="service-wrap">
                        <div class="service-img">
                            <img src="<?php echo base_url();?>assets/uploads/post/<?php echo $row->ft_post;?>" alt=" " class="img-responsive">
                        </div>
                        <div class="service-content">
                            <h4><?php echo $row->jdl_post;?></h4>
							<p><?php $words = explode(" ", $row->isi_post);
							$katanya = implode(" ", array_splice($words,0,11));
							echo $katanya;
							?> . . . <a href="<?php echo base_url();?>home/post/<?php echo $row->id_post?>">Lanjutkan</a></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
        </div>
    </div>
    <!-- service-area end -->
    <!-- .brand-area start -->
    <div class="brand-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-3">
                    <div class="brand-wrap text-center">
                        <a href="https://www.pkbtv.com/" target="_blank"><img src="https://cdn.pkb.id/banners/pkb-tv.png" alt="" draggable="false"></a>
                    </div>
                </div>
                <div class="col-sm-2 col-3">
                    <div class="brand-wrap text-center">
                        <a href="https://www.pkb.id/" target="_blank"><img src="https://cdn.pkb.id/banners/perempuan-bangsa.png" alt="" draggable="false"></a>
                    </div>
                </div>
                <div class="col-sm-2 col-3">
                    <div class="brand-wrap text-center">
                        <a href="https://www.gemasabajatim.org/" target="_blank"><img src="https://cdn.pkb.id/banners/gema-asaba.png" alt="" draggable="false"></a>
                    </div>
                </div>
                <div class="col-sm-2 col-3">
                    <div class="brand-wrap text-center">
                        <a href="https://www.pkb.id/" target="_blank"><img src="https://cdn.pkb.id/banners/garda-bangsa.png" alt="" draggable="false"></a>
                    </div>
                </div>
                <div class="col-sm-3 col-3">
                    <div class="brand-wrap text-center">
                        <a href="https://www.fraksipkb.com/" target="_blank"><img src="https://cdn.pkb.id/banners/fpkb-dprri.png" alt="" draggable="false"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .brand-area end -->
    <!-- google map area start -->
  
    <!-- google map area end -->