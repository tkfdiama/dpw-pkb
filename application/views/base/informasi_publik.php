<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" type="text/css" href="<?php echo base_url()?>assets/img/icon.png">
<title>DPW PKB JAWA TIMUR</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Plumpungrejo Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="<?php echo base_url();?>assets/home/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url();?>assets/home/css/style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script type="text/javascript" src="<?php echo base_url();?>assets/home/js/jquery-2.1.4.min.js"></script>
<script src="<?php echo base_url();?>assets/home/js/main.js"></script>

<!-- //js -->
<!-- font-awesome icons -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/home/css/font-awesome.min.css" />

<!-- //font-awesome icons -->
<link href="//fonts.googleapis.com/css?family=Alegreya:400,400i,700,700i,900,900i&subset=latin-ext" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
</head>
	
<body>
<!-- banner -->
	<div class="header-w3l">
		<div class="container">
			<nav class="navbar navbar-default">
				<div class="navbar-header navbar-left">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="sr-only">Toggle navigation </span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div class="wthree_logo">
						<h1><a class="navbar-brand" href="<?php echo base_url();?>home">DPW PKB JAWA TIMUR</a></h1>
					</div>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
					<nav>
						<ul class="nav navbar-nav link-effect-5" id="link-effect-5">
							<li><a href="<?php echo base_url();?>home" data-hover="Home">Home</a></li>
							<li><a href="<?php echo base_url();?>home/about" data-hover="About">About</a></li>
							
							<li><a href="<?php echo base_url();?>home/gallery" data-hover="Gallery">Gallery</a></li>
							<li><a href="<?php echo base_url();?>home/informasi_publik" data-hover="Informasi Publik">Informasi Publik</a></li>
							<li><a href="<?php echo base_url();?>home/katalog" data-hover="Katalog">Katalog</a></li>
							<li><a href="<?php echo base_url();?>home/berita" data-hover="berita">Berita</a></li>
						</ul>
						<div class="w3_social_icons">
							<div class="cd-main-header">
								<a href="https://www.instagram.com/dpwpkbjatim/" class="fa fa-instagram" style="font-size: 40px;"></a>
							</div>
						</div>
						<div class="clearfix"></div>
					</nav>
				</div>
			</nav>
		</div>
	</div>
	<div class="banner1">

	</div>
<!-- //banner -->	
<!-- about -->

<div class="about_page">
	<div class="container">
		<h3 class="agile">Informasi Publik</h3>
		<div id="parentVerticalTab">
		<ul>
           <li><a href="<?php echo base_url();?>assets/doc/tes.pdf" target="_blank">Doc</a></li></ul>
        </div>
	</div>
</div>

<!-- //about -->	
<!-- footer -->
	<div class="footer"> 
		<div class="container"> 
			<div class="col-md-3 w3_footer_grid">
				<h3>Dewan Pengurus</h3>
		    <li><a href="<?php echo base_url();?>dewan" data-hover="dewan">Dewan Pengurus</a></li>
		    (https://infopemilu.kpu.go.id/pileg2019/verpol/lengkap/117)
			</div>
		<div class="container"> 
			<div class="col-md-3 w3_footer_grid">
				<h3>Anggota Legislatif</h3>
		    <li><a href="<?php echo base_url();?>legislatif" data-hover="legislatif">Anggota Legislatif</a></li>
			</div>
		<div class="container"> 
			<div class="col-md-3 w3_footer_grid">
				<h3>PPID</h3>
		    <li><a href="<?php echo base_url();?>ppid" data-hover="ppid">PPID</a></li>
			</div>
		<div class="container"> 
			<div class="col-md-3 w3_footer_grid">
				<h3>LHKPN</h3>
		    <li><a href="<?php echo base_url();?>lhkpn" data-hover="lhkpn">LHKPN</a></li>
			</div>
			</div>
			<div class="col-md-3 w3_footer_grid">
				<h3>Laporan Keuangan</h3>
		    <li><a href="<?php echo base_url();?>uang" data-hover="uang">Laporan Keuangan</a></li>
			</div>
	
			<div class="col-md-3 w3_footer_grid">
				<h3>Peraturan Partai</h3>
		    <li><a href="<?php echo base_url();?>partai" data-hover="partai">Peraturan Partai</a></li>
			</div>
		
			<div class="col-md-3 w3_footer_grid">
				<h3>Asas dan Prinsip Perjuangan</h3>
		    <li><a href="<?php echo base_url();?>asas" data-hover="asas">Asas dan Prinsip Perjuangan</a></li>
			</div>
		<div class="col-md-3 w3_footer_grid">
				<h3>Struktur Organisasi</h3>
		    <li><a href="<?php echo base_url();?>struktur" data-hover="struktur">Struktur Organisasi</a></li>
			</div>
			</div>


			<div class="clearfix"> </div>
			<div class="w3layouts_footer_grid">
				<div class="w3layouts_footer_grid_left">
					<h2><a href="index.html">DPW PKB JAWA TIMUR</a></h2>
				</div>
				<div class="w3layouts_footer_grid_right">
					<p>Feby Fitriani Sudarsono 2019</a></p>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>	
	
<!-- //footer -->
<!-- for bootstrap working -->
	<script src="<?php echo base_url();?>assets/home/js/bootstrap.js"></script>
<!-- //for bootstrap working -->
		
</body>
</html>