    <!-- breadcumb-area start -->
    <div class="breadcumb-area black-opacity bg-img-2">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                        <h2>Makna Lambang</h2>
                        <ul>
                            <li><a href="<?php echo base_url();?>home">Home/</a></li>
                            <li>Tentang PKB/</li>
                            <li>Makna Lambang</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcumb-area end -->
    <!-- blog-area start -->
    <div class="blog-area blog-details-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="blog-wrap">
                        <div class="blog-content">
                            <h4>Makna Lambang</h4>
                        </div>
                    </div>
                    <div class="blog-details-wrap">
                        <p style="text-align:left"> Lambang Partai terdiri dari bola dunia yang dikelilingi sembilan bintang dengan tulisan nama partai pada bagian bawah, dengan bingkai dalam empat persegi bergaris ganda, dan tulisan PKB di bawahnya yang diberi bingkai luar dengan garis tunggal.</p>
			 			<p style="text-align:left"> Makna Lambang :</p>
						<p ><strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1. Arti Gambar adalah sebagai berikut :</strong></p>
						<p style="text-align:left">	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1. Bumi dan peta Indonesia, bermakna tanah air Indonesia yang merupakan basis perjuangan Partai dalam usahanya untuk mencapai tujuan partai.</p>
						<p style="text-align:left">	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2. Sembilan bintang bermakna idealisme partai yang memuat 9 (sembilan) nilai, yaitu kemerdekaan, keadilan, kebenaran, kejujuran, kerakyatan, persamaan, kesederhanaan, keseimbangan, dan persaudaraan.</p>
						<p style="text-align:left">	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3. Tulisan nama Partai dan singkatannya bermakna identitas diri partai yang berfungsi sebagai sarana perjuangan aspirasi politik rakyat Indonesia yang memiliki kehendak menciptakan tatanan kehidupan bangsa yang demokratis.</p>
						<p style="text-align:left">	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4. Bingkai segi empat dengan garis ganda yang sejajar bermakna garis perjuangan Partai yang menempatkan orientasi duniawi dan ukhrawi, material dan spiritual, lahir dan batin, secara sejajar.</p>
						<p ><strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2. Arti warna adalah sebagai berikut: </strong></p>
						<p style="text-align:left">	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1. Putih, bermakna kesucian, ketulusan dan kebenaran yang menjadi etos perjuangan partai.</p>
						<p style="text-align:left">	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2. Hijau, bermakna kemakmuran lahir dan batin bagi seluruh rakyat Indonesia yang menjadi tujuan perjuangan.</p>
						<p style="text-align:left">	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3. Kuning, bermakna kebangkitan Bangsa yang menjadi nuansa pembaharuan dan berpijak pada kemaslahatan umat manusia.</p>
						<p style="text-align:left">	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4. Asas dan Prinsip Perjuangan (menggantikan Tugas dan fungsi)</p>
						<p style="text-align:justify">	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5. Partai berasaskan Ketuhanan Yang Maha Esa, kemanusiaan yang adil dan beradab, persatuan Indonesia, kerakyatan yang dipimpin oleh hikmah kebijaksanaan dalam permusyawaratan/perwakilan, dan keadilan sosial bagi seluruh rakyat Indonesia. Sedangkan Prinsip perjuangan PKB adalah pengabdian kepada Allah Subhanahu wa Ta’ala, menjunjung tinggi kebenaran dan kejujuran, menegakkan keadilan, menjaga persatuan, menumbuhkan persaudaraan dan kebersamaan sesuai dengan nilai-nilai Islam Ahlusunnah Waljama’ah.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- blog-area end -->