    <!-- breadcumb-area start -->
    <div class="breadcumb-area black-opacity bg-img-2">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                    <?php foreach($media as $row): ?>
                        <h2>Media <?php echo $row->kategori?></h2>
                        <ul>
                            <li><a href="<?php echo base_url();?>home">Home/</a></li>
                            <li>Media/</li>
                            <li> <?php echo $row->kategori?></li>
                        </ul>
                    <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcumb-area end -->
    <!-- blog-area start -->
    <div class="blog-area">
        <div class="container">
            <div class="row md-revarce-wrap revarce-wrap">
                <div class="col-lg-4 col-md-6 col-12">
                    <aside class="sidebar-wrap">
                        <div class="widget sidebar-menu">
                            <h3 class="widget-title">Kategori Media</h3>
                            <ul>
                                <li><a href="<?php echo base_url();?>media/press">Press Release</a></li>
                                <li><a href="<?php echo base_url();?>media/pidato">Pidato</a></li>
                            </ul>
                        </div>
                    </aside>
                </div>
                <div class="col-lg-8">
                    <div class="row">
                    <?php foreach($media as $row): ?>
                        <div class="col-sm-6 col-12">
                            <div class="blog-wrap">
                                <div class="blog-img">
                                    <img src="<?php echo base_url();?>assets/uploads/media/<?php echo $row->ft_media;?>" alt="" class="img-responsive">
                                </div>
                                <div class="blog-content">
                                    <ul class="blog-meta">
                                        <li><a href="#"><?php echo $row->up_media;?></a></li>
                                        <li>|</li>
                                    </ul>
                                    <h4><a href="<?php echo base_url();?>home/media/<?php echo $row->id_press?>"><?php echo $row->judul;?></a></h4>
                                    <p><?php $words = explode(" ", $row->isi);
                                    $katanya = implode(" ", array_splice($words,0,11));
                                    echo $katanya;
                                    ?> . . . </p>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
<!--                         <div class="col-12">
                            <div class="pagination-wrap text-center">
                                <ul>
                                    <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                                    <li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li> <span>3</span></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                                </ul>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- blog-area end -->