    <!-- breadcumb-area start -->
    <div class="breadcumb-area black-opacity bg-img-2">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                        <h2>Naskah Deklarasi</h2>
                        <ul>
                            <li><a href="<?php echo base_url();?>home">Home/</a></li>
                            <li>Tentang PKB/</li>
                            <li>Naskah Deklarasi</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcumb-area end -->
    <!-- blog-area start -->
    <div class="blog-area blog-details-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="blog-wrap">
                        <div class="blog-content">
                            <h4>Naskah Deklarasi</h4>
                        </div>
                    </div>
                    <div class="blog-details-wrap">
                        <p style="text-align:justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bahwa cita-cita proklamasi kemerdekaan bangsa Indonesia adalah terwujudnya suatu bangsa yang merdeka, bersatu, adil dan makmur, serta untuk mewujudkan pemerintahan Negara Kesatuan Republik Indonesia yang melindungi segenap bangsa Indonesia dan untuk memajukan kesejahteraan umum, mencerdaskan kehidupan bangsa, serta ikut melaksanakan ketertiban dunia yang berdasarkan kemerdekaan, perdamaian abadi dan keadilan sosial.</p>
                        <p style="text-align:justify">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bahwa wujud dari bangsa yang dicita-citakan itu adalah masyarakat beradab dan sejahtera, yang mengejawantahkan nilai-nilai kejujuran, kebenaran, kesungguhan dan keterbukaan yang bersumber dari hati nurani, bisa dipercaya, setia dan tepat janji serta mampu memecahkan masalah sosial yang bertumpu pada kekuatan sendiri, bersikap dan bertindak adil dalam segala situasi, tolong menolong dalam kebajikan, serta konsisten menjalankan garis/ketentuan yang telah disepakati bersama.</p>
                        <blockquote style="text-align:justify">Bahwa perwujudan dari cita-cita kemerdekaan tersebut menghendaki tegaknya demokrasi yang menjamin terciptanya tatanan kenegaraan yang adil serta pemerintahan yang bersih dan terpercaya, terjaminnya hak-hak asasi manusia, dan lestarinya lingkungan hidup bagi peningkatan harkat dan martabat bangsa Indonesia yang diridlai Allah Subhanahu wa Ta’ala. Bahwa untuk mewujudkan hal tersebut, diperlukan adanya wahana perjuangan yang kuat, mampu menyalurkan aspirasi dan menyatukan seluruh potensi bangsa yang majemuk, serta terlibat aktif dalam penyelenggaraan negara dengan berakhlaqul karimah. Maka dengan memohon rahmat, taufiq, hidayah, dan inayah Allah Subhanahu wa Ta’ala, didirikanlah PARTAI KEBANGKITAN BANGSA yang bersifat <b>kebangsaan, demokratis dan terbuka.</b></blockquote>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- blog-area end -->