    <!-- breadcumb-area start -->
    <div class="breadcumb-area black-opacity bg-img-2">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                        <h2>Struktur Organisasi</h2>
                        <ul>
                            <li><a href="<?php echo base_url();?>home">Home/</a></li>
                            <li>Tentang PKB/</li>
                            <li>Struktur Organisasi</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcumb-area end -->
    <!-- blog-area start -->
    <div class="blog-area blog-details-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="blog-wrap">
                        <div class="blog-content">
                            <h4>Struktur Organisasi</h4>
                        </div>
                    </div>
                    <div class="blog-details-wrap">
			 			<p style="text-align:left"> Struktur Organisasi Partai terdiri dari:</p>
						<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1. Organisasi Tingkat Pusat, dipimpin oleh Dewan Pengurus Pusat, disingkat DPP.</p>
						<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2. Organisasi Daerah Provinsi, dipimpin oleh Dewan Pengurus Wilayah, disingkat DPW.</p>
						<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3. Organisasi Daerah Kabupaten/Kota, dipimpin oleh Dewan Pengurus Cabang, disingkat DPC.</p>
						<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4. Organisasi Tingkat Kecamatan, dipimpin oleh Dewan Pengurus Anak Cabang, disingkat DPAC.</p>
						<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5. Organisasi Tingkat Desa/Kelurahan atau yang setingkat, dipimpin oleh Dewan Pengurus Ranting, disingkat DPRt.</p>
						<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6. Organisasi Tingkat Dusun/Lingkungan/Kawasan Pemukiman, dipimpin oleh Dewan Pengurus Anak Ranting, disingkat DPARt.</p>
						<blockquote>Untuk Perwakilan Partai di luar negeri, dapat dibentuk struktur organisasi Partai setingkat Dewan Pengurus Cabang, yaitu Dewan Pengurus Cabang Perwakilan, disingkat DPCP.</blockquote>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- blog-area end -->