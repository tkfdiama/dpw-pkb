    <!-- breadcumb-area start -->
    <div class="breadcumb-area black-opacity bg-img-2">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                        <h2>Visi Misi</h2>
                        <ul>
                            <li><a href="<?php echo base_url();?>home">Home/</a></li>
                            <li>Tentang PKB/</li>
                            <li>Visi Misi</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcumb-area end -->
    <!-- blog-area start -->
<div class="testmonial-area testmonial-area2 ptb-120">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2 col-12">
                    <div class="test-active">
                        <div class="test-wrap2 text-center">
                            <i class="fa fa-quote-left"></i>
                            <p style="text-align:justify"><b>1.</b>&nbsp;Mewujudkan cita-cita kemerdekaan Republik Indonesia sebagaimana dituangkan dalam Pembukaan Undang-Undang Dasar 1945.<br>
							<b>2.</b>&nbsp;&nbsp;&nbsp;Mewujudkan masyarakat yang adil dan makmur secara lahir dan batin, material dan spiritual.<br>
							<b>3.</b>&nbsp;&nbsp;&nbsp;Mewujudkan tatanan politik nasional yang demokratis, terbuka, bersih dan berakhlakul karimah.</p>
                            <h4>Visi</h4>
                        </div>
                        <div class="test-wrap2 text-center">
                            <i class="fa fa-quote-right"></i>
                            <p style="text-align:justify"><b>1.</b>&nbsp;Bidang Ekonomi: menegakkan dan mengembangkan kehidupan ekonomi kerakyatan yang adil dan demokratis.<br>
							<b>2.</b>&nbsp;Bidang Hukum: berusaha menegakkan dan mengembangkan negara hukum yang beradab, mampu mengayomi seluruh rakyat, menjunjung tinggi hak-hak asasi manusia, dan berkeadilan sosial.<br>
							<b>3.</b>&nbsp;Bidang Sosial Budaya: berusaha membangun budaya yang maju dan modern dengan tetap memelihara jatidiri bangsa yang baik demi meningkatkan harkat dan martabat bangsa.<br>
							<b>4.</b>&nbsp;Bidang Pendidikan: berusaha meningkatkan kualitas sumber daya manusia yang berakhlak mulia, mandiri, terampil, profesional dan kritis terhadap lingkungan sosial di sekitarnya, mengusahakan terwujudnya sistem pendidikan nasional yang berorientasi kerakyatan, murah dan berkesinambungan.<br>
							<b>5.</b>&nbsp;Bidang Pertahanan: membangun kesadaran setiap warga negara terhadap kewajiban untuk turut serta dalam usaha pertahanan negara; mendorong terwujudnya swabela masyarakat terhadap perlakuan-perlakuan yang menimbulkan rasa tidak aman, baik yang datang dari pribadi-pribadi maupun institusi tertentu dalam masyarakat.</p>
                            <h4>Misi</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- blog-area end -->