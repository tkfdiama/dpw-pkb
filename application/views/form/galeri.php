    <style>
        #image-holder {
            margin-top: 8px;
        }
        
        #image-holder img {
            border: 8px solid #DDD;
            max-width:50%;
        }
    </style>
        <div class="right_col" role="main">
          <div class="">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> Galeri <small></small></h2>
                      <ul class="nav navbar-right panel_toolbox">
                        <li><button class="btn btn-primary" onclick="javascript:tambah();"> Tambah <i class="fa fa-plus"></i></button></a>
                        </li>
                      </ul>
                  <div class="clearfix"></div>
                </div>
                  <div class="x_content">

                    <div class="row" id="detail">
                      <?php foreach ($galeri as $row): ?>
                      <div class="col-md-55">
                        <div class="thumbnail">
                          <div class="image view view-first">
                            <img style="width: 100%; display: block;" src="<?php echo base_url();?>assets/uploads/galeri/<?php echo $row->name_gal;?>" alt="image" />
                            <div class="mask">
                              <p><?php echo $row->capt_gal;?></p>
                              <div class="tools tools-bottom">
                                <button style="background: none; border: none;" onclick="javascript:hapus('mgaleri/delete/<?php echo $row->id_gal; ?>/<?php echo $row->name_gal; ?>');"><i class="fa fa-times"></i></button>
                              </div>
                            </div>
                          </div>
                          <div class="caption">
                            <p><?php echo $row->capt_gal;?></p>
                          </div>
                        </div>
                      </div>
                     <?php endforeach; ?> 
                    </div>
              <div class="row" id="form-tambah" style="display: none;">
                <form class="form-horizontal"  method="post" id="detail-tambah" name="detail-tambah" enctype="multipart/form-data">
                  <input type="hidden" name="id_gal" id="id_gal">
                  <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <label for="tiga" class="col-sm-2 control-label"> Caption </label>
                      <div class="col-md-10 col-sm-10 col-xs-10">
                        <input type="text" class="form-control" placeholder="caption" name="capt_gal" id="capt_gal" required>
                      </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <label for="tiga" class="col-sm-2 control-label"> Foto </label>
                      <div class="col-md-10 col-sm-10 col-xs-10">
                        <input type="hidden" name="fotonya" id="fotonya">
                        <input type="file" accept="image/*" name="name_gal" class="form-control" id="foto" multiple>
                                <div id="image-holder">
                                  <?php
                                    if(isset($_GET['id']))
                                        echo "<img src='../img/$data[2].'?rand='".rand()."' alt=''>";
                                    ?>
                                </div>
                                <script>
                                    $("#foto").on('change', function () {

                                        //Get count of selected files
                                        var countFiles = $(this)[0].files.length;

                                        var imgPath = $(this)[0].value;
                                        var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
                                        var image_holder = $("#image-holder");
                                        image_holder.empty();

                                        var x = document.getElementById("foto");
                                        var file = x.files[0];

                                        if (extn == "png" || extn == "jpg" || extn == "jpeg" || extn == "gif") {
                                            if (typeof (FileReader) != "undefined") {

                                                //loop for each file selected for uploaded.
                                                for (var i = 0; i < countFiles; i++) {

                                                    var reader = new FileReader();
                                                    reader.onload = function (e) {
                                                        $("<img />", {
                                                            "src": e.target.result,
                                                            "class": "thumb-image"
                                                        }).appendTo(image_holder);
                                                    }

                                                    image_holder.show();
                                                    reader.readAsDataURL($(this)[0].files[i]);
                                                }

                                            } else {
                                                alert("This browser does not support FileReader.");
                                            }
                                        } else {
                                            alert("hanya boleh foto bertype PNG, JPG dan GIF");
                                            var control = $("#foto");
                                            control.replaceWith(control.val('').clone(true));
                                        }
                                    });
                  
                                </script>
                      </div>
                  </div>
                  <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 pull-right">
                          <button class="btn btn-primary" type="button" onclick="javascript:cancel();">Cancel</button>
                          <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit" class="btn btn-success" onclick="javascript:simpan('mgaleri/coba_insert');" id="save" name="save">Submit</button>
                        </div>
                      </div>
                  </form>
                </div>
                  </div>
                </div>
          </div>
        </div>
        <script type="text/javascript">
function tambah(){
    $('#detail').hide();
    $('#form-tambah').show();
}
function cancel(){
    $('#detail').show();
    $('#form-tambah').hide();
}
function simpan(url){
        $('#save').val('saving . . ');
        $('#save').attr('disabled',true);
        $("#detail-tambah").click(function(evt){
            evt.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url: "<?php echo base_url()?>" + url,
                type: 'POST',
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                enctype: 'multipart/form-data',
                processData: false,
                success: function (response) {
                    alert("Data berhasil masuk");
                    document.location="<?php echo base_url()?>" + 'admin/galeri';
                }
            });
            return false;
        });
    }
function hapus(url){
    $.ajax({
        url : "<?php echo base_url()?>" + url,
        type : 'post',
        dataType : 'json',
        success : function(data)
        {
            if(data.status == 'ok')
            {
                alert("Data berhasil dihapus");
                location.reload();
            }
        },
        error : function(res)
        {
            show_message('Gagal',(res.responseText));
        }
    });
}
function ubah(url){
    $.ajax({
        url : "<?php echo base_url()?>" + url ,
        type : 'post',
        dataType : 'json',
        success : function(data)
        {
            $('#detail').hide();
            // $.fillToForm("#detail-tambah", data.data);
            $('#id_gal').val(data.data.id_gal);
            $('#capt_gal').val(data.data.capt_gal);
            $('#name_gal').val(data.data.name_gal);
            $('#fotonya').val(data.data.name_gal);
            $('#form-tambah').show();
        },
        error : function(res)
        {
            show_message('Gagal',(res.responseText));
        }
    });
}
</script>