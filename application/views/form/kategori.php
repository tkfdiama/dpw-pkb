 <!-- <script src="<?php echo base_url();?>assets/dash/js/app.min.js" type="text/javascript"></script> -->
          <!-- page content -->
      <div class="right_col" role="main">
      <div class="">
          <div class="x_panel">
              <div class="x_title">
                <h2> Master Kategori <small>form input </small></h2>
                <ul class="nav navbar-right panel_toolbox">
                      <li><button class="btn btn-primary" onclick="javascript:tambah();"> Tambah <i class="fa fa-plus"></i></button></a>
                      </li>
                    </ul>
                <div class="clearfix"></div>
              </div>
                    <div class="row" id="detail">
                      <div class="col-sm-12">
                        <div class="card-box table-responsive">
                          <table id="datatable-keytable" class="table table-striped table-bordered">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Nama</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php $a=1; foreach ($kategori as $row): ?>
                            <tr>
                                <td><?php echo $a; ?></td>
                                <td><?php echo $row->nm_kat; $a++; ?></td>
                                <td>
                                    <button type="button" data-title='Delete' data-toggle='modal' onclick="javascript:hapus('mkategori/delete/<?php echo $row->id_kat; ?>');" class="btn btn-danger pull-right"> Hapus</button>
                                    <button type="button" data-title='Edit' onclick="javascript:ubah('mkategori/get_detail/<?php echo $row->id_kat; ?>')" class="btn btn-primary pull-right"> Edit</button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                <div class="row" id="form-tambah" style="display: none;">
                <form class="form-horizontal"  method="post" id="detail-tambah" name="detail-tambah" enctype="multipart/form-data">
                  <input type="hidden" name="id_kat" id="id_kat">
                  <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                      <label for="tiga" class="col-sm-2 control-label"> Nama kategori </label>
                      <div class="col-md-10 col-sm-10 col-xs-10">
                        <input type="text" class="form-control" placeholder="Nama" name="nm_kat" id="nm_kat" required>
                      </div>
                  </div>
                  <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 pull-right">
                          <button class="btn btn-primary" type="button" onclick="javascript:cancel();">Cancel</button>
                          <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit" class="btn btn-success" onclick="javascript:simpan('mkategori/coba_insert');" id="save" name="save">Submit</button>
                        </div>
                      </div>
                  </form>
                </div>
              </div>
            </div>
          <div class="clearfix"></div>
        </div>
      <!-- end page content -->
<script type="text/javascript">
function tambah(){
    $('#detail').hide();
    $('#form-tambah').show();
}
function cancel(){
    $('#detail').show();
    $('#form-tambah').hide();
}
function simpan(url){
        $('#save').val('saving . . ');
        $('#save').attr('disabled',true);
        $("#detail-tambah").click(function(evt){
            evt.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url: "<?php echo base_url()?>" + url,
                type: 'POST',
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                enctype: 'multipart/form-data',
                processData: false,
                success: function (response) {
                    alert("Data berhasil masuk");
                    document.location="<?php echo base_url()?>" + 'admin/kategori';
                }
            });
            return false;
        });
    }
function hapus(url){
    $.ajax({
        url : "<?php echo base_url()?>" + url,
        type : 'post',
        dataType : 'json',
        success : function(data)
        {
            if(data.status == 'ok')
            {
                alert("Data berhasil dihapus");
                location.reload();
            }
        },
        error : function(res)
        {
            show_message('Gagal',(res.responseText));
        }
    });
}
function ubah(url){
    $.ajax({
        url : "<?php echo base_url()?>" + url ,
        type : 'post',
        dataType : 'json',
        success : function(data)
        {
            $('#detail').hide();
            // $.fillToForm("#detail-tambah", data.data);
            $('#id_kat').val(data.data.id_kat);
            $('#nm_kat').val(data.data.nm_kat);
            $('#ft_ktg').val(data.data.ft_ktg);
            $('#fotonya').val(data.data.ft_ktg);
            $('#form-tambah').show();
        },
        error : function(res)
        {
            show_message('Gagal',(res.responseText));
        }
    });
}
</script>