<div class="breadcumb-area black-opacity bg-img-2">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcumb-wrap">
                    <?php foreach ($postnya as $row): ?>
                        <h2>Berita <?php echo $row->nm_kat?></h2>
                        <ul>
                            <li><a href="<?php echo base_url();?>home">Home/</a></li>
                            <li>Berita/</li>
                            <li>Berita <?php echo $row->nm_kat?>/</li>
                            <li><?php echo $row->jdl_post?></li>
                    <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcumb-area end -->
    <!-- blog-area start -->
   
<?php foreach ($postnya as $row): ?>
<div class="about_page">
	<div class="container">
		<h3 class="agile" style="text-align: left;"><?php echo $row->jdl_post;?></h3>
		<h5 style="color: #aa9d9d;">Di Publikasikan : <?php echo $row->up_post?></h5>
		<div id="parentVerticalTab">
            <div class="resp-tabs-container hor_1">
                <div>
					<div class="col-md-7" style="margin-left: 20%;">
						 <img style="width: 100%; display: block;" src="<?php echo base_url();?>assets/uploads/post/<?php echo $row->ft_post;?>" alt="image" />
					</div>
					<div class="clearfix"></div>
						<?php echo $row->isi_post;?>
                </div>     
            </div>
        </div>
	</div>
</div>
<?php endforeach;?>

<!-- //about -->	
<!-- footer -->
<!-- for bootstrap working -->
	<script src="<?php echo base_url();?>assets/home/js/bootstrap.js"></script>
<!-- //for bootstrap working -->

</body>
</html>
