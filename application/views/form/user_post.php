 <!-- <script src="<?php echo base_url();?>assets/dash/js/app.min.js" type="text/javascript"></script> -->
 <style>
        #image-holder {
            margin-top: 8px;
        }
        
        #image-holder img {
            border: 8px solid #DDD;
            max-width:50%;
        }
    </style>
          <!-- page content -->
      <div class="right_col" role="main">
      <div class="">
          <div class="x_panel">
              <div class="x_title">
                <h2> User Post </h2>
                <div class="clearfix"></div>
              </div>
                    <div class="row" id="detail">
                      <div class="col-sm-12">
                        <div class="card-box table-responsive">
                          <table id="datatable-keytable" class="table table-striped table-bordered">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>Judul</th>
                                <th>Isi</th>
                                <th>Harga</th>
                                <th>Tanggal post</th>
                                <th>Kategori</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php $a=1; foreach ($posting as $row): ?>
                            <tr>
                                <td><?php echo $a; ?></td>
                                <td><?php echo $row->jdl_pst;?></td>
                                <td><?php echo $row->isi_pst;?></td>
                                <td><?php echo $row->hrg_pst;?></td>
                                <td><?php echo $row->id_ktg;?></td>
                                <td><?php echo $row->nm_ktg; $a++; ?></td>
                                <td><?php 
                                    if( $row->status_pst==1)echo "<div class='btn btn-success btn-xs'>aktif</div>";
                                    else echo "<div class='btn btn-danger btn-xs'>Tidak aktif</div>";
                                    ?></td>
                                <td>
                                    <?php
                                    if( $row->status_pst==1){?>
                                    <button type="button" data-title='Delete' data-toggle='modal' onclick="javascript:hapus('mkategori/delete/<?php echo $row->id_ktg; ?>/<?php echo $row->ft_pst; ?>');" class="btn btn-danger pull-right"> Hapus</button>
                                    <button type="button" data-title='Lihat' onclick="javasript:ubah('mkategori/get_detail/<?php echo $row->id_ktg; ?>')" class="btn btn-primary pull-right"> Lihat </button>
                                   <?php } ?>
                                </td>
                            </tr>
                              <?php endforeach; ?>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
              </div>
            </div>
          <div class="clearfix"></div>
        </div>
      <!-- end page content -->
<script type="text/javascript">
function simpan(url){
        $('#save').val('saving . . ');
        $('#save').attr('disabled',true);
        $("#detail-tambah").click(function(evt){
            evt.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url: "<?php echo base_url()?>" + url,
                type: 'POST',
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                enctype: 'multipart/form-data',
                processData: false,
                success: function (response) {
                    alert("Data berhasil masuk");
                    document.location="<?php echo base_url()?>" + 'home/kategori';
                }
            });
            return false;
        });
    }
function hapus(url){
    $.ajax({
        url : "<?php echo base_url()?>" + url,
        type : 'post',
        dataType : 'json',
        success : function(data)
        {
            if(data.status == 'ok')
            {
                alert("Data berhasil dihapus");
                location.reload();
            }
        },
        error : function(res)
        {
            show_message('Gagal',(res.responseText));
        }
    });
}
</script>