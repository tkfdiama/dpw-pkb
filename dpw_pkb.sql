-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 12, 2019 at 11:04 AM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dpw_pkb`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_adm` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `ft_adm` varchar(250) DEFAULT NULL,
  `status_adm` smallint(6) DEFAULT NULL,
  `last_used` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_adm`, `username`, `password`, `ft_adm`, `status_adm`, `last_used`) VALUES
(1, 'admin', '4a7d1ed414474e4033ac29ccb8653d9b', 'profil.png', 1, '2019-02-08 13:22:30'),
(2, 'admin1', '4a7d1ed414474e4033ac29ccb8653d9b', '1532300581IMG-20180722-WA0047.jpg', 1, '2018-08-09 07:42:54'),
(3, 'feby', 'admin', '1532300581IMG-20180722-WA0047.jpg', 1, '2018-08-09 07:42:54'),
(4, 'fea', 'fea', NULL, NULL, NULL),
(5, 'Fea', 'beeb6599d25d6427a0568180846fc9a8', '0', 1, '2019-01-23 01:49:49'),
(6, 'feby', '4a7d1ed414474e4033ac29ccb8653d9b', '1548224774da.PNG', 1, '2019-02-11 01:34:46');

-- --------------------------------------------------------

--
-- Table structure for table `download`
--

CREATE TABLE `download` (
  `id_download` int(10) NOT NULL,
  `file` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `download`
--

INSERT INTO `download` (`id_download`, `file`) VALUES
(2, '1549950302Jurnal Pemodelan SAMSAT.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `galeri`
--

CREATE TABLE `galeri` (
  `id_gal` int(11) NOT NULL,
  `id_adm` int(11) DEFAULT NULL,
  `name_gal` varchar(250) NOT NULL,
  `capt_gal` varchar(250) DEFAULT NULL,
  `post_gal` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galeri`
--

INSERT INTO `galeri` (`id_gal`, `id_adm`, `name_gal`, `capt_gal`, `post_gal`) VALUES
(33, 6, '1549470679bg.PNG', 'nyoba', '2019-02-06 10:31:19'),
(34, 6, '0', 'Masukkan foto yang paling mengesankan dan tidak ada yang salah, ayo', '2019-02-11 19:19:37'),
(35, 6, '1549934418nyoba.PNG', 'Masukkan foto yang paling mengesankan dan tidak ada yang salah, ayo', '2019-02-11 19:20:18');

-- --------------------------------------------------------

--
-- Table structure for table `halaman_depan`
--

CREATE TABLE `halaman_depan` (
  `id_hal` int(11) NOT NULL,
  `jdl_hal` varchar(250) DEFAULT NULL,
  `isi_hal` varchar(1000) DEFAULT NULL,
  `ft_hal` varchar(250) DEFAULT NULL,
  `status_hal` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `halaman_depan`
--

INSERT INTO `halaman_depan` (`id_hal`, `jdl_hal`, `isi_hal`, `ft_hal`, `status_hal`) VALUES
(1, 'About', '<font color=\"#222222\" face=\"sans-serif\"><span style=\"font-size: 14px;\">Waduk Dawuhan terletak di Desa Plumpungrejo dan Desa Sidomulyo, Kecamatan Wonoasri, Kabupaten Madiun, Jawa Timur Jarak tempuh ± 8 Km dari Kota Caruban. Waduk dawuhan ini memiliki luas 1.273 hektar. Selain menjadi tempat wisata waduk ini digunakan warga setempat untuk pengairan sawah yang ada di 9 desa di 3 kecamatan, yaitu Kecamatan Wonoasri, Kecamatan Balerejo, dan Madiun</span></font><br>', 'bg.png', 2),
(2, 'Coba judul lagin', 'ilwilwhrlwhr', '1532838196IMG-20180722-WA0055.jpg', 2),
(3, 'Coba judul', '<p><b>test isi&nbsp;</b></p>', '1532972061IMG-20180722-WA0066.jpg', 2),
(4, 'DPW PKB JAWA TIMUR', 'Berlokasi di&nbsp;<span style=\"color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: small;\">JL. Ketintang Madya, 153 - 155, Karah, Jambangan, Kota SBY, Jawa Timur 60232.</span>', '1548086549dpw.PNG', 1);

-- --------------------------------------------------------

--
-- Table structure for table `katalog`
--

CREATE TABLE `katalog` (
  `id_ktl` int(11) NOT NULL,
  `id_adm` int(11) NOT NULL,
  `ft_ktl` varchar(500) NOT NULL,
  `ket_ktl` varchar(500) NOT NULL,
  `tlp_ktl` varchar(13) NOT NULL,
  `post_ktl` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `katalog`
--

INSERT INTO `katalog` (`id_ktl`, `id_adm`, `ft_ktl`, `ket_ktl`, `tlp_ktl`, `post_ktl`) VALUES
(7, 1, '1534008938IMG-20180720-WA0014.jpg', 'coba caption', '30930193', '2018-08-11 12:35:38'),
(8, 1, '1534008963IMG-20180721-WA0014.jpg', 'coba lagi', '40190491491', '2018-08-11 12:36:03'),
(9, 1, '1534245144IMG_20180814_054936_083.jpg', 'Kerajinan Tali Kur', '085396952004', '2018-08-14 11:12:24');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id_kat` int(11) NOT NULL,
  `id_adm` int(11) DEFAULT NULL,
  `nm_kat` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id_kat`, `id_adm`, `nm_kat`) VALUES
(2, NULL, 'Headline'),
(3, NULL, 'Utama'),
(4, NULL, 'Parlemen');

-- --------------------------------------------------------

--
-- Table structure for table `legislatif`
--

CREATE TABLE `legislatif` (
  `id_legislatif` int(11) NOT NULL,
  `file` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `legislatif`
--

INSERT INTO `legislatif` (`id_legislatif`, `file`) VALUES
(13, '1549875805DPR-RI.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `media_center`
--

CREATE TABLE `media_center` (
  `id_press` int(11) NOT NULL,
  `id_adm` int(11) DEFAULT NULL,
  `kategori` varchar(20) DEFAULT NULL,
  `judul` varchar(250) DEFAULT NULL,
  `isi` varchar(1000) DEFAULT NULL,
  `ft_media` varchar(100) DEFAULT NULL,
  `up_media` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media_center`
--

INSERT INTO `media_center` (`id_press`, `id_adm`, `kategori`, `judul`, `isi`, `ft_media`, `up_media`) VALUES
(15, 6, 'press release', 'nyoba press', 'presssssssssssssssss', '1549869231dpw.PNG', '2019-02-11 01:13:51'),
(16, 6, 'pidato', 'pidato', 'pidato donggggggggggggggg', '1549869258favicon.PNG', '2019-02-11 01:14:18');

-- --------------------------------------------------------

--
-- Table structure for table `pengurus`
--

CREATE TABLE `pengurus` (
  `id_pengurus` int(11) NOT NULL,
  `file` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengurus`
--

INSERT INTO `pengurus` (`id_pengurus`, `file`) VALUES
(1, '1549937216Jurnal Pemodelan SAMSAT.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id_post` int(11) NOT NULL,
  `id_adm` int(11) DEFAULT NULL,
  `id_kat` int(11) DEFAULT NULL,
  `jdl_post` varchar(250) DEFAULT NULL,
  `isi_post` varchar(550) DEFAULT NULL,
  `ft_post` varchar(250) DEFAULT NULL,
  `up_post` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id_post`, `id_adm`, `id_kat`, `jdl_post`, `isi_post`, `ft_post`, `up_post`) VALUES
(33, 6, 2, 'PKB Jatim Optimis', 'DPW PKB JAWA TIMUR&nbsp;', '1549876395nyoba.PNG', '2019-02-11 03:13:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_adm`);

--
-- Indexes for table `download`
--
ALTER TABLE `download`
  ADD PRIMARY KEY (`id_download`);

--
-- Indexes for table `galeri`
--
ALTER TABLE `galeri`
  ADD PRIMARY KEY (`id_gal`),
  ADD KEY `FK_RELATIONSHIP_4` (`id_adm`);

--
-- Indexes for table `halaman_depan`
--
ALTER TABLE `halaman_depan`
  ADD PRIMARY KEY (`id_hal`);

--
-- Indexes for table `katalog`
--
ALTER TABLE `katalog`
  ADD PRIMARY KEY (`id_ktl`),
  ADD KEY `fk_admin_katalog` (`id_adm`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kat`),
  ADD KEY `FK_RELATIONSHIP_3` (`id_adm`);

--
-- Indexes for table `legislatif`
--
ALTER TABLE `legislatif`
  ADD PRIMARY KEY (`id_legislatif`);

--
-- Indexes for table `media_center`
--
ALTER TABLE `media_center`
  ADD PRIMARY KEY (`id_press`);

--
-- Indexes for table `pengurus`
--
ALTER TABLE `pengurus`
  ADD PRIMARY KEY (`id_pengurus`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id_post`),
  ADD KEY `FK_RELATIONSHIP_1` (`id_adm`),
  ADD KEY `FK_RELATIONSHIP_2` (`id_kat`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_adm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `download`
--
ALTER TABLE `download`
  MODIFY `id_download` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `galeri`
--
ALTER TABLE `galeri`
  MODIFY `id_gal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `halaman_depan`
--
ALTER TABLE `halaman_depan`
  MODIFY `id_hal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `katalog`
--
ALTER TABLE `katalog`
  MODIFY `id_ktl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `legislatif`
--
ALTER TABLE `legislatif`
  MODIFY `id_legislatif` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `media_center`
--
ALTER TABLE `media_center`
  MODIFY `id_press` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `pengurus`
--
ALTER TABLE `pengurus`
  MODIFY `id_pengurus` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id_post` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `galeri`
--
ALTER TABLE `galeri`
  ADD CONSTRAINT `FK_RELATIONSHIP_4` FOREIGN KEY (`id_adm`) REFERENCES `admin` (`id_adm`);

--
-- Constraints for table `katalog`
--
ALTER TABLE `katalog`
  ADD CONSTRAINT `fk_admin_katalog` FOREIGN KEY (`id_adm`) REFERENCES `admin` (`id_adm`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kategori`
--
ALTER TABLE `kategori`
  ADD CONSTRAINT `FK_RELATIONSHIP_3` FOREIGN KEY (`id_adm`) REFERENCES `admin` (`id_adm`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `FK_RELATIONSHIP_1` FOREIGN KEY (`id_adm`) REFERENCES `admin` (`id_adm`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_RELATIONSHIP_2` FOREIGN KEY (`id_kat`) REFERENCES `kategori` (`id_kat`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
